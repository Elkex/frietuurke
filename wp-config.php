<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'frietuurke' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'P/d4XR-~YL_4n<#9^Lx5-8SidKpIPtVf5,iw7rbrBS94g_}4a3O*ozscuL2vvwd9' );
define( 'SECURE_AUTH_KEY',  'v,./`Vro`>URB%eYF,GJSwY9V$2d|KgDuDT/I!4TNL9h)DT{<O}g?jki#[A;*735' );
define( 'LOGGED_IN_KEY',    'o;dT.Yc50?C{eP;9|Z|1R6@BPA@=j|R~t|9Q+cLj1}:Osl{xI `3Q<)08G52X,]o' );
define( 'NONCE_KEY',        'zV3*&u{/;UYzSc,Sm%6;6-I3p^noy< LmD7mBk1k9`3z~#,fblqAx)T<b-UPa?yz' );
define( 'AUTH_SALT',        '1;Eb*x%~P>vF.5]GN5X-bI_Cj//TC8xuc(wJvz6;;]S#b=F`,}PeAZ<b9tX]]H*M' );
define( 'SECURE_AUTH_SALT', '=,9{9eb-Ely/N;WG)-$=:@W`;GfZ7YsobdAb`76=ubwq6x2O^Y9Xh_UhmX7jfxx|' );
define( 'LOGGED_IN_SALT',   'f0+MsQN&M[^0lrUNlr1YD(Np,GSIf6s)Q^bB?6WRD3Q)M@Jp8O{/9fm|N[+uVB0B' );
define( 'NONCE_SALT',       'O`$7frY2$%D[<ylN!7aK+-+0mVA*6ec!5(*S&%>6U*=-ez%k?d 3B2x6zc4Zs8~p' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
