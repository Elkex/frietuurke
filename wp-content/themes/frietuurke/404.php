<?php get_template_part('partials/head'); ?>
<?php get_template_part('partials/nav'); ?>

<main class="c-main" role="main">
    <div class="g-container">
        <div class="e-text-container e-text-center g-m-y-xxl">
            <a href="<?php echo get_site_url(); ?>" class="g-inline-block">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/logo_frietuurke.svg" alt="Logo" width="300" height="auto">
            </a>
            <h1 class="g-m-top-lg">
                Pagina niet gevonden
            </h1>
            <p>
                De pagina die je zoekt bestaat helaas niet.
            </p>
            <a href="/" class="wp-block-button__link has-primary-background-color g-m-top-md">
                Terug naar de homepagina
            </a>
        </div>
    </div>
</main>

<?php get_template_part('partials/footer'); ?>