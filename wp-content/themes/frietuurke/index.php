<?php get_template_part('partials/head'); ?>

<?php get_template_part('partials/nav'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <main class="c-main" role="main">
            <section class="g-m-y-xxl">
                <div class="g-container">
                    <h1 class="e-text-center">
                        <?php
                        echo the_title();
                        ?>
                    </h1>
                    <div class="g-grid">
                        <div class="g-col g-col-12">
                            <div class="g-p-x-lg">
                                <?php
                                echo the_content();
                                ?>
                            </div>
                        </div>
                    </div>
            </section>
        </main>

    <?php endwhile; ?>
<?php endif; ?>

<?php get_template_part('partials/footer'); ?>