<?php
//register settings
function theme_options_add()
{
    register_setting('theme_settings', 'theme_settings');
}

//add settings page to menu
function add_options()
{
    add_menu_page(__('Theme Options'), __('Theme Options'), 'manage_options', 'settings', 'theme_options_page');
}
//add actions
add_action('admin_init', 'theme_options_add');
add_action('admin_menu', 'add_options');

//start settings page
function theme_options_page()
{

    if (!isset($_REQUEST['updated']))
        $_REQUEST['updated'] = false;

    //get variables outside scope
    global $color_scheme;

?>
    <div>
        <form method="post" action="options.php">
            <h2>Theme Options</h2>
            <?php settings_fields('theme_settings'); ?>
            <?php $options = get_option('theme_settings'); ?>
            <table>
                <tr valign="top">
                    <th align="left">
                        <?php _e('Adres'); ?>
                    </th>
                </tr>
                <tr>
                    <td>
                        <textarea id="theme_settings[adres]" name="theme_settings[adres]" rows="5" cols="36"><?php esc_attr_e($options['adres']); ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="theme_settings[adres]"><?php _e('Enter adres'); ?>
                        </label>
                    </td>
                </tr>
            </table>
            <table style="margin-top: 2rem;">
                <tr valign="top">
                    <th align="left">
                        <?php _e('Telefoonnummer'); ?>
                    </th>
                </tr>
                <tr>
                    <td>
                        <input id="theme_settings[phone]" type="text" size="36" name="theme_settings[phone]" value="<?php esc_attr_e($options['phone']); ?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="theme_settings[phone]"><?php _e('Voeg je telefoonnummer toe'); ?>
                        </label>
                    </td>
                </tr>
            </table>
            <table style="margin-top: 2rem;">
                <tr valign="top">
                    <th align="left">
                        <?php _e('Telefoonnummer 2'); ?>
                    </th>
                </tr>
                <tr>
                    <td>
                        <input id="theme_settings[phone2]" type="text" size="36" name="theme_settings[phone2]" value="<?php esc_attr_e($options['phone2']); ?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="theme_settings[phone2]"><?php _e('Voeg je tweede telefoonnummer toe'); ?>
                        </label>
                    </td>
                </tr>
            </table>
            <table style="margin-top: 2rem;">
                <tr valign="top">
                    <th align="left">
                        <?php _e('Facebook URL'); ?>
                    </th>
                </tr>
                <tr>
                    <td>
                        <input id="theme_settings[facebookurl]" type="text" size="36" name="theme_settings[facebookurl]" value="<?php esc_attr_e($options['facebookurl']); ?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="theme_settings[facebookurl]"><?php _e('Enter Facebook URL'); ?>
                        </label>
                    </td>
                </tr>
            </table>
            <table style="margin-top: 2rem;">
                <tr valign="top">
                    <th align="left">
                        <?php _e('Instagram URL'); ?>
                    </th>
                </tr>
                <tr>
                    <td>
                        <input id="theme_settings[instagramurl]" type="text" size="36" name="theme_settings[instagramurl]" value="<?php esc_attr_e($options['instagramurl']); ?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="theme_settings[instagramurl]"><?php _e('Enter Instagram URL'); ?>
                        </label>
                    </td>
                </tr>
            </table>
            <table style="margin-top: 2rem;">
                <tr valign="top">
                    <th align="left">
                        <?php _e('Twitter URL'); ?>
                    </th>
                </tr>
                <tr>
                    <td>
                        <input id="theme_settings[twitterurl]" type="text" size="36" name="theme_settings[twitterurl]" value="<?php esc_attr_e($options['twitterurl']); ?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="theme_settings[twitterurl]"><?php _e('Enter Twitter URL'); ?>
                        </label>
                    </td>
                </tr>
            </table>
            <div style="margin-top: 2rem;">
                <input name="submit" id="submit" value="Save Changes" type="submit" class="button button-primary">
            </div>
        </form>

    </div><!-- END wrap -->

<?php
}
?>