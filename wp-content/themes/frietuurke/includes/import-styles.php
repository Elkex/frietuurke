<?php

// LOAD STYLES
function kleurcode_styles()
{
    wp_register_style('kleurcode_style', get_template_directory_uri() . '/assets/dist/css/styles.css', array(), '1.0', 'all');
    //wp_register_style('kleurcode_styles', get_template_directory_uri() . '/dist/css/style.css', array(), '1.0', 'all');
    wp_enqueue_style('kleurcode_style'); 
    //wp_enqueue_style('kleurcode_styles');
}

add_action('wp_enqueue_scripts', 'kleurcode_styles');