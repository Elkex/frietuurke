# Korabase Frontend Framework for Korazon

The Korabase frontend framework is a framework to create and build .twig, JS, CSS and image assets to a fully usable deployment package.

## NVM installation

### Uninstall Node

First of all you need to uninstall Node JS on your computer before you can install the Node Version Manager for a more flexible way to switch your Node JS versions.

This will have an impact on what Gulp JS version you are using.

### Windows installation

You can find all the information in the Windows installation of the Windows Node Version Manager here:
https://github.com/coreybutler/nvm-windows

### MAC OS installation

You can find all the information on the Mac installation of the Mac Nove Version Manager here:
https://github.com/nvm-sh/nvm

### Installing node versions

You can check if the Node version manager is correctly installed by using:
```bash
node -v
```

Your current node version will be returned like this (this might be different from newer Node versions):

```bash
v12.13.1
```

You can install a new node version by using the following command:

```bash
nvm install <node version>
```

We recommend installing an older Node version like version 10.17 for older Gulp versions:

```bash
nvm install 10.17
```

You can get an overview of all installed node versions by using the command:

```bash
nvm list
```

Also make sure you have your Bash / Node shell as the active terminal in for instance Visual Code Studio.

## Installing Gulp to use the Gulp command

```bash
npm install gulp -g
```

## Installing the Korabase
To get started, you need to install the Korabase project in the Korabase folder. Just run:

```bash
npm install
```

## Running the Korabase project

### You can run the Korabase project via the Gulp command in your terminal

```bash
gulp
```

### You can run also run the Korabase project via the npm start command in your terminal

```bash
npm start
```

## Gulp settings

If you go to the gulp folder and open the _settings.js folder, you will see different settings that are being used to determine where specific files will be saved and called.

### Settings production

First of all we will determine whether our application is building in the production or development environment. The difference between production and development is whether commands in css and javascript should be deleted etc.

```javascript
    production: true, // True = production mode, false = development mode
```

### Setting Browsersync location

It is important to set the Browsersync location. BrowserSync will automatically reload your page whenever make changes to the Twig templates, SCSS or JS files.

```javascript
  browserSync: {
    url: '<Here comes the route to the dist folder of this project>' // example:  ~Desktop/sites/www/korabase
  },
```

If your path is correct, your browser will open whenever you run the gulp or npm start command at this url:
```bash
  http://localhost:3000
```

You can always navigate to this url when your gulp preprocessor is running.

## Add icons to template

You can add SVG icons to a template by adding it to the src/assets/sprite folder.

If you want to show the icon on a page, you can use the following snippet:

```html
  	<svg class='c-icon c-icon-<icon-name>'>
		<use xlink:href='./assets/sprite/sprite.svg#<icon-name>'></use>
	</svg>
```
Here is an example of the Facebook icon that is beind loaded into the template.

```html
  	<svg class='c-icon c-icon-facebook'>
		<use xlink:href='./assets/sprite/sprite.svg#facebook'></use>
	</svg>
```