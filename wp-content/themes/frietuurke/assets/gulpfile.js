/**
 * A simple Gulp 4 Starter Kit for modern web development.
 *
 * @inpsiration https://github.com/jr-cologne/gulp-starter-kit GitHub Repository
 *
 * ________________________________________________________________________________
 *
 * gulpfile.js
 *
 * The gulp configuration file.
 *
 */

const gulp = require( 'gulp' );

require( './gulp/_task.scss.js' );
require( './gulp/_task.js.js' );
require( './gulp/_task.images.js' );
require( './gulp/_task.vendor.js' );
require( './gulp/_task.sprite.js' );
require( './gulp/_task.fonts.js' );
require( './gulp/_task.twig.js' );
require( './gulp/_task.server.js' );
require( './gulp/_task.watch.js' );
require( './gulp/_task.build.js' );

gulp.task( 'default', gulp.series( 'build', gulp.parallel( 'serve', 'watch' ) ) );

gulp.task( 'serve', gulp.series('build','watch') );

