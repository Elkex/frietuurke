settings = {

	production: true,

	browserSync: {
		url: 'http://frietuurke.test/'
	},

	twig: {
		src: './src/twig/',
		dist: './dist/',
		build: true
	},

	scss: {
		src: './src/scss/',
		dist: './dist/css/',
		filename: 'styles.css',
		autoprefixer: {
			overrideBrowserslist: [
				"> 1%",
				"last 3 versions",
				"IE 11",
				"IE 10"
			]
		}
	},

	js: {
		src: './src/js/',
		components: './src/js/components/',
		dist: './dist/js/',
		filename: 'theme.js'
	},

	vendor: {
		src: './src/vendor/',
		dist: './dist/vendor/'
	},

	images: {
		src: './src/img/',
		dist: './dist/img/'
	},

	sprite: {
		src: './src/sprite/',
		dist: './dist/sprite/',
		filename: 'sprite.svg'
	},

	fonts: {
		src: './src/fonts/',
		dist: './dist/fonts/'
	},

};

module.exports.settings = settings;