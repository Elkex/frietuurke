const gulp = require('gulp'),
      del = require('del'),

      settings = require('./_settings').settings,

      imagemin = require('gulp-imagemin');

gulp.task('images:clear', () => {
  return del(settings.images.dist);
})

gulp.task('images:build', () => {

  return gulp.src([ settings.images.src + '**/*.+(png|jpg|jpeg|gif|svg|ico)' ],
    { since: gulp.lastRun('images:build')
  })

  .pipe(imagemin())
  .pipe(gulp.dest(settings.images.dist))
});

gulp.task('images', gulp.series('images:clear', 'images:build'))
