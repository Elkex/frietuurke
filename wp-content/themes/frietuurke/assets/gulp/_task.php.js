const gulp = require( 'gulp' ),
  del = require( 'del' ),
  settings = require( './_settings' ).settings,
  twig = require( 'gulp-twig' );

gulp.task( 'twig:clear', () => {
  return del( settings.twig.dist + '*.twig' );
} )

gulp.task( 'twig', () => {

  return gulp.src( [ settings.twig.src + '**/!(_)*.twig' ] )
    .pipe( twig() )
    .pipe( gulp.dest( settings.twig.dist ) )
} )

//gulp.task( 'twig', gulp.series( 'twig:clear', 'twig:build' ) )
