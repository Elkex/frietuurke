require('./_settings')

const gulp = require('gulp'),
  sourcemaps = require('gulp-sourcemaps'),
  babel = require('gulp-babel'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  gulpif = require('gulp-if'),
  stripDebug = require('gulp-strip-debug');

gulp.task('js', () => {
  
  return gulp.src([settings.js.src + '**/*.js'],[settings.js.components + '**/*.js'], { since: gulp.lastRun('js') })
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(gulpif(settings.production, stripDebug()))
    .pipe(concat(settings.js.filename))
    .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(settings.js.dist))
});
