const gulp = require('gulp');

require( './_task.clear.js' );

gulp.task( 'build', gulp.series( 'clear', gulp.parallel( 'scss', 'js', 'twig', 'images', 'sprite', 'fonts', 'vendor' ) ) );
