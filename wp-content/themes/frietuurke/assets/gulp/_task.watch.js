const gulp = require( 'gulp' ),
settings = require( './_settings' ).settings;

require( './_task.twig' );
require( './_task.scss.js' );
require( './_task.js.js' );
require( './_task.images.js' );
require( './_task.vendor.js' );
require( './_task.sprite.js' );
require( './_task.fonts.js' );
require( './_task.twig.js' );
require( './_task.watch.js' );
require( './_task.server.js' );

gulp.task( 'watch', () => {

    const watchTwig = [
      settings.twig.src + '**/*.twig',
    ]
  
    const watchScss = [
      settings.scss.src + '**/*.scss',
    ]
  
    const watchJs = [
      settings.js.src + '**/*.+js'
    ]
  
    const watchImages = [
      settings.images.src + '**/*.+(png|jpg|jpeg|gif|svg|ico)'
    ];
  
    const watchSprite = [
      settings.sprite.src + '**/*svg'
    ];
  
    const watchFonts = [
      settings.sprite.src + '**/*.{ttf,otf,eot,svg,woff,woff2}'
    ]
  
    const watchVendor = [
      settings.vendor.src + '**/*.js'
    ];
   
    const watchPhp = [
      './**/*.php',
    ];
  
    // settings.vendor.src.forEach(dependency => {
    //   watchVendor.push(settings.vendor.dist + dependency + '/**/*.*');
    // });
    gulp.watch( watchScss, gulp.series( 'scss', 'reload' ) );
  
    gulp.watch( watchJs, gulp.series( 'js', 'reload' ) );
    gulp.watch( watchTwig, gulp.series( 'twig', 'reload' ) );
  
    gulp.watch( watchImages, gulp.series( 'images:build' ) );
    gulp.watch( watchSprite, gulp.series( 'sprite' ) );
    gulp.watch( watchFonts, gulp.series( 'fonts:build' ) );
    gulp.watch( watchVendor, gulp.series( 'vendor:build' ) );
    
  
  } );