const settings = require( './_settings' ).settings;

const gulp = require( 'gulp' ),
	gulpif = require( 'gulp-if' ),
	concat = require( 'gulp-concat' ),
	sourcemaps = require( 'gulp-sourcemaps' ),
	gulpSass = require( "gulp-sass" ),
	nodeSass = require( "node-sass" ),
	sass = gulpSass( nodeSass ),
	autoprefixer = require( 'autoprefixer' ),
	cssnano = require( 'cssnano' ),
	sortMediaQueries = require( 'postcss-sort-media-queries' ),
	postcss = require( 'gulp-postcss' );

gulp.task( 'scss', () => {

	return gulp.src( [
		settings.scss.src + '**/*.sass',
		settings.scss.src + '**/*.scss',
	] )

		.pipe( gulpif( !settings.production, sourcemaps.init() ) )
		.pipe( sass().on( 'error', sass.logError ) )
		.pipe(
			sass()
		)
		.pipe( concat( settings.scss.filename ) )

		.pipe( postcss(
			[
				sortMediaQueries( {
					sort: 'mobile-first'
				} ),
				autoprefixer( settings.scss.autoprefixer ),
				cssnano(),
			]
		) )



		.pipe( sourcemaps.write( '.' ) )

		.pipe( gulp.dest( settings.scss.dist ) )

} );