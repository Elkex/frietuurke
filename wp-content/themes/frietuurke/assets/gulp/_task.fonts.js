const gulp = require('gulp'),
      del = require('del'),

      settings = require('./_settings').settings;

gulp.task('fonts:clear', function () {
    return del(settings.fonts.dist);
});

gulp.task('fonts:build', function () {
    return gulp.src([
      settings.fonts.src + '**/*.{ttf,otf,eot,svg,woff,woff2}'],
      { since: gulp.lastRun('fonts:build') })
    .pipe(gulp.dest(settings.fonts.dist))
});

gulp.task('fonts', gulp.series('fonts:clear', 'fonts:build'))
