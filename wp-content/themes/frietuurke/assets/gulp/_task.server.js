const settings = require( './_settings' ).settings;

// Dependencies
const gulp = require('gulp'),
browserSync = require( 'browser-sync');

// Task
// const server = browserSync.create();

gulp.task( 'serve', (cb) => {
    browserSync.init( {
        /*server: {
            baseDir: [ settings.browserSync.url ]
        },*/
        proxy: {
            target: settings.browserSync.url
        },
        notify: false
    } );
    cb();
})

gulp.task( 'reload', (cb) => {
    browserSync.reload();
    cb();
})