const gulp = require('gulp'),
      del = require('del'),
      settings = require('./_settings').settings,
      sprite = require('gulp-svg-sprite');

gulp.task('sprite:clear', () => {
  return del(settings.vendor.dist);
})

gulp.task('sprite:build', () => {
  return gulp.src([settings.sprite.src + '**/*.svg'], {
    base: settings.sprite.src,
    since: gulp.lastRun('sprite:build')
  })
  .pipe(sprite({
    mode: {
      symbol: {
        dest: './',
        sprite: settings.sprite.filename,
      }
    },
    shape: {
      id: {
        generator: '%s'
      }
    },
    svg: {
      xmlDeclaration: false,
      doctypeDeclaration: false
    }
  }))
  .pipe(gulp.dest(settings.sprite.dist))
});

gulp.task('sprite', gulp.series('sprite:clear', 'sprite:build'))
