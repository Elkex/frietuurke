accordion = {
    init: function () {
        jQuery('.c-faq-item-title').click(function(e) {
            e.preventDefault();
          let jQuerythis = jQuery(this);
          if (jQuerythis.next().hasClass('c-faq-item-active')) {
              jQuerythis.next().removeClass('c-faq-item-active');
              jQuerythis.removeClass('c-faq-item-active');
              jQuerythis.next().slideUp(350);
          } else {
              jQuery('.c-faq-item-content').removeClass('c-faq-item-active');
              jQuery('.c-faq-item-title').removeClass('c-faq-item-active');
              jQuery('.c-faq-item-content').slideUp(350);
              jQuerythis.next().toggleClass('c-faq-item-active');
              jQuerythis.toggleClass('c-faq-item-active');
              jQuerythis.next().slideToggle(350);
          }
      });
    }
}