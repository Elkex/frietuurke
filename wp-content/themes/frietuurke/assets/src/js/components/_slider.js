slider = {
    init: function () {
        jQuery('.wp-block-kleurcode-core-slideshow .wp-block-gallery').slick({
            infinite: false,
            speed: 300,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
        });
    }
}