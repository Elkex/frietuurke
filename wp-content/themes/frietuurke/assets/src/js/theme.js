jQuery(document).ready(function ($) {
    accordion.init();
    counter.init();
    fade.init();
    masonry.init();
    navigation.init();
    tabs.init();
    slider.init();
    AOS.init({
        once: true
     })
});