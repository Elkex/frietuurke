<?php get_template_part('partials/head'); ?>
<?php get_template_part('partials/nav'); ?>

<main class="c-main" role="main">
<article class="g-m-y-xxl">
	<div class="g-container">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				<?php get_the_post_thumbnail(); ?>
				<h1>
					<?php echo the_title(); ?>
				</h1>
				<div class="g-m-y-xl">
					<?php echo the_content(); ?>
				</div>
				<h4>
					<?php echo the_author(); ?>
				</h4>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</article>

</main>

<?php get_template_part('partials/footer'); ?>