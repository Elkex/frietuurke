<?php get_template_part('partials/head'); ?>
<?php get_template_part('partials/nav'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <main class="c-main" role="main">
            <?php the_content(); ?>
        </main>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_template_part('partials/footer'); ?>