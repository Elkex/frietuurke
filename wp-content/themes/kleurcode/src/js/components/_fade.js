fade = {
    init : function() {
        var elements;
        var windowHeight;
        
        function init() {
            elements = document.querySelectorAll('.fx');
            windowHeight = window.innerHeight;
        }
        
        function checkPosition() {
            for (var i = 0; i < elements.length; i++) {
                var element = elements[i];
                var positionFromTop = elements[i].getBoundingClientRect().top + 150;
                
                if (positionFromTop - windowHeight <= 0) {
                    element.classList.add('fx-end');
                    element.classList.remove('fx');
                }
            }
        }
        
        window.addEventListener('scroll', checkPosition);
        window.addEventListener('resize', init);
        
        init();
        checkPosition();
    }
}