let counter = {
    init: function () {

        var counters = document.querySelectorAll('.c-counter');
        var start = 0;

        const setCounterAnimation = (counter) => {
            var number = parseInt(counter.getElementsByClassName('c-counter-placeholder')[0].innerHTML)
            if (number > 1000) {
                startNum = parseInt(number - ((1 / 100) * number))
                animateValue(counter.getElementsByClassName('c-counter-number')[0], startNum, number, 1);
            } else if (number > 100) {
                startNum = parseInt(number - ((10 / 100) * number))
                animateValue(counter.getElementsByClassName('c-counter-number')[0], startNum, number, 150);
            }
            else {
                animateValue(counter.getElementsByClassName('c-counter-number')[0], 0, numberWithCommas(number), 300);
            }
        }

        const animateValue = (object, start, end, duration) => {
            var range = end - start;
            var current = start;
            var increment = end > start ? 1 : -1;
            var stepTime = Math.abs(Math.floor(duration / range));
            var obj = object
            var timer = setInterval(function () {
                current += increment;
                obj.innerHTML = numberWithCommas(current);
                if (current == end) {
                    clearInterval(timer);
                }
            }, stepTime);
        }

        const numberWithCommas = (x) => {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }

        [...counters].forEach(counter => setCounterAnimation(counter));
    }
}

