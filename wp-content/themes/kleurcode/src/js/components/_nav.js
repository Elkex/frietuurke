navigation = {
    init: function(){
        jQuery('.p-nav-toggle').click(function(e){
            jQuery('body').toggleClass('body-overflow');
            jQuery('.p-subnav-wrapper button').removeClass('p-subnav-active-btn');
            jQuery('.p-nav-subnav').removeClass('p-subnav-active');
        })
        
        jQuery('.p-subnav-wrapper button').click(function(e){
            e.preventDefault();
            if(jQuery(this).hasClass('p-subnav-active-btn')){ 
                jQuery('.p-subnav-wrapper button').removeClass('p-subnav-active-btn');
                jQuery('.p-nav-subnav').removeClass('p-subnav-active');
            }else{
                jQuery('.p-subnav-wrapper button').removeClass('p-subnav-active-btn');
                jQuery('.p-nav-subnav').removeClass('p-subnav-active');
                jQuery(this).addClass('p-subnav-active-btn');
                jQuery(this).parent().siblings('.p-nav-subnav').addClass('p-subnav-active');
            }   
        })

        jQuery(window).resize(function(){
            jQuery('body').removeClass('body-overflow');
            jQuery('.p-subnav-wrapper button').removeClass('p-subnav-active-btn');
            jQuery('.p-nav-subnav').removeClass('p-subnav-active');
        })
    }
}