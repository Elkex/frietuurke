let tabs = {
    init: function () {

        jQuery('.wp-block-kleurcode-core-tab:first-child').addClass('active');
        jQuery('.wp-block-kleurcode-core-tabs .c-tab-nav-item:first-child').addClass('active');

        jQuery(".c-tab-nav-item").click(function (e) { 

            e.preventDefault();

            jQuery('.wp-block-kleurcode-core-tab').removeClass('active');
            jQuery('.c-tab-nav-item').removeClass('active');

            jQuery('#' + e.target.getAttribute('data-tab')).addClass('active');
            jQuery(this).addClass('active');
        })

    }
}