jQuery(document).ready(function ($) {
    accordion.init();
    counter.init();
    fade.init();
    masonry.init();
    navigation.init();
    tabs.init();
    AOS.init({
        once: true
     })
});