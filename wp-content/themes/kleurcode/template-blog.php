<?php
/*
 * Template Name: Template Blog
 */
?>
<?php get_template_part('partials/head'); ?>
<?php get_template_part('partials/nav'); ?>

<main class="c-main" role="main">
    <section class="g-m-y-xl">
        <div class="g-container">
            <h1 class="e-text-center g-m-bottom-xxl">
                <?php echo the_title(); ?>
            </h1>
            <?php echo the_content(); ?>
            <div class="g-grid">
                <?php
                $pages = 1;
                $temp = $wp_query;
                $wp_query = null;
                $wp_query = new WP_Query();
                $wp_query->query('posts_per_page=6' . '&paged=' . $paged);
                while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                    <article class="g-col g-col-12 g-col-4-lg g-flex g-flex-column">
                        <div class="c-card g-flex-grow g-flex g-flex-column g-flex-justify-content-between g-flex-align-items-start">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="c-card-thumbnail">
                                <?php echo get_the_post_thumbnail(); ?>
                            </a>
                            <div class="g-p-lg g-flex-grow g-flex g-flex-column g-flex-justify-content-between g-flex-align-items-start">
                                <h3>
                                    <?php the_title(); ?>
                                    </a>
                                </h3>
                                <?php the_excerpt(); ?>
                                <a href="<?php the_permalink(); ?>" class="c-button c-button-primary g-m-top-md">
                                    Lees meer
                                </a>
                            </div>
                        </div>
                    </article>

                <?php endwhile; ?>
                <?php if ($paged > 1) : ?>
                    <div class="g-col g-col-12">
                        <nav class="g-flex g-flex-justify-content-between g-m-y-xl">
                            <?php previous_posts_link('&laquo; Nieuwere posts '); ?>

                            <?php next_posts_link('Oudere posts &raquo;'); ?>
                        </nav>
                    </div>
                <?php else : ?>
                    <div class="g-col g-col-12">
                        <nav class="g-flex g-flex-justify-content-end g-m-y-xl">
                            <?php next_posts_link('Oudere posts &raquo;'); ?>
                        </nav>
                    </div>

                <?php endif; ?>

                <?php wp_reset_postdata(); ?>

            </div>
        </div>
    </section>
</main>
<?php get_template_part('partials/footer'); ?>