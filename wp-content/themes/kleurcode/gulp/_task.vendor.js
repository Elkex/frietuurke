const gulp = require('gulp'),
      del = require('del'),
      util = require('gulp-util'),
      plumber = require('gulp-plumber'),
      browserSync = require('browser-sync').get('Dev Server'),

      settings = require('./_settings').settings;

gulp.task('vendor:clear', () => {
  return del(settings.vendor.dist);
})

gulp.task('vendor:build', () => {

  // if (settings.vendor.src.length === 0) {
  //   return new Promise((resolve) => {
  //     console.log("No dependencies specified");
  //     resolve();
  //   });
  // }
  //
  // return gulp.src(settings.vendor.src.map(dependency => './node_modules/' + dependency + '/**/*.*'), {
  //   base: settings.vendor.src,
  //   since: gulp.lastRun('vendor')
  // })
  //
  // .pipe(gulp.dest(settings.vendor.dist))
  // .pipe(browserSync.stream());

  return gulp.src(settings.vendor.src + '*.js')
    .pipe(plumber(function (error) {
      util.log(util.colors.red('Error (' + error.plugin + '): ' + error.message));
      this.emit('end');
    }))
    .pipe(gulp.dest(settings.vendor.dist))
    .pipe(browserSync.stream());

});

gulp.task('vendor', gulp.series('vendor:clear', 'vendor:build'))
