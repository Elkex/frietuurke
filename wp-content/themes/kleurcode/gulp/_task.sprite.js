const gulp = require('gulp'),
      del = require('del'),
      util = require('gulp-util'),
      plumber = require('gulp-plumber'),
      browserSync = require('browser-sync').get('Dev Server'),

      settings = require('./_settings').settings,

      sprite = require('gulp-svg-sprite');

gulp.task('sprite:clear', () => {
  return del(settings.vendor.dist);
})

gulp.task('sprite:build', () => {
  return gulp.src([settings.sprite.src + '**/*.svg'], {
    base: settings.sprite.src,
    since: gulp.lastRun('sprite:build')
  })
  .pipe(plumber(function (error) {
    util.log(util.colors.red('Error (' + error.plugin + '): ' + error.message));
    this.emit('end');
  }))
  .pipe(sprite({
    mode: {
      symbol: {
        dest: './',
        sprite: settings.sprite.filename,
      }
    },
    shape: {
      id: {
        generator: '%s'
      }
    },
    svg: {
      xmlDeclaration: false,
      doctypeDeclaration: false
    }
  }))
  .pipe(gulp.dest(settings.sprite.dist))
  .pipe(browserSync.stream());
});

gulp.task('sprite', gulp.series('sprite:clear', 'sprite:build'))
