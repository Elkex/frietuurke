settings = {

  production: false,

  browserSync: {
    url: 'http://kleurcode-wordpress.test/'
  },

  scss: {
    src: './src/css/',
    dist: './dist/css/',
    filename: 'style.css',
    autoprefixer: {
      overrideBrowserslist: [
        "> 1%",
        "last 3 versions",
        "IE 11",
        "IE 10"
      ]
    }
  },

  js: {
    src: './src/js/',
    components: './src/js/components/',
    dist: './dist/js/',
    filename: 'main.js'
  },

  vendor: {
    src: './src/vendor/',
    dist: './dist/vendor/'
  },

  images: {
    src: './src/img/',
    dist: './dist/img/'
  },

  sprite: {
    src: './src/sprite/',
    dist: './dist/sprite/',
    filename: 'sprite.svg'
  },

  fonts: {
    src: './src/fonts/',
    dist: './dist/fonts/'
  },

};

module.exports.settings = settings;