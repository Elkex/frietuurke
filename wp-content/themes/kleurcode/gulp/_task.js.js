require('./_settings')

const gulp = require('gulp'),
  util = require('gulp-util'),
  plumber = require('gulp-plumber'),
  browserSync = require('browser-sync').get('Dev Server'),
  sourcemaps = require('gulp-sourcemaps'),
  babel = require('gulp-babel'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  gulpif = require('gulp-if'),
  stripDebug = require('gulp-strip-debug');

gulp.task('js', () => {
  
  return gulp.src(settings.js.src + '**/**/*.js')

    .pipe(plumber(function (error) {
      util.log(util.colors.red('Error (' + error.plugin + '): ' + error.message));
      this.emit('end');
    }))

    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(gulpif(settings.production, stripDebug()))
    .pipe(concat(settings.js.filename))
    //.pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(settings.js.dist))
    .pipe(browserSync.stream());
});