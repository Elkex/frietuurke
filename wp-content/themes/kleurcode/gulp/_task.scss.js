require('./_settings')

const gulp = require('gulp'),
      del = require('del'),
      util = require('gulp-util'),
      plumber = require('gulp-plumber'),
      browserSync = require('browser-sync').get('Dev Server'),
      gulpif = require('gulp-if'),
      concat = require('gulp-concat'),
      sourcemaps = require('gulp-sourcemaps'),
      sass = require('gulp-sass'),
      cached = require('gulp-cached'),
      sassPartialsImported = require('gulp-sass-partials-imported'),

      autoprefixer = require('autoprefixer'),
      cssnano = require('cssnano'),
      postcss = require('gulp-postcss');

gulp.task('scss:clear', () => {
  return del(settings.scss.dist);
})

gulp.task('scss:build', () => {

  return gulp.src([
    settings.scss.src + '**/*.sass',
    settings.scss.src + '**/*.scss'
  ], { since: gulp.lastRun('scss:build') })
  
    .pipe(plumber(function (error) {
      util.log(util.colors.red('Error (' + error.plugin + '): ' + error.message));
      this.emit('end');
    }))

    .pipe(cached('sassfiles'))
    .pipe(sassPartialsImported(settings.scss.src))

    .pipe(gulpif(!settings.production, sourcemaps.init()))

    .pipe(sass().on('error', sass.logError))

    .pipe(postcss([ autoprefixer(settings.scss.autoprefixer), cssnano() ]))

    .pipe(concat(settings.scss.filename))

    .pipe(sourcemaps.write('.'))
    
    .pipe(gulp.dest(settings.scss.dist))
    
    .pipe(browserSync.stream())
});

gulp.task('scss', gulp.series('scss:clear', 'scss:build'))
