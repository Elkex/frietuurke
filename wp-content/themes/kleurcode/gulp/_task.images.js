const gulp = require('gulp'),
      del = require('del'),
      util = require('gulp-util'),
      plumber = require('gulp-plumber'),
      browserSync = require('browser-sync').get('Dev Server'),

      settings = require('./_settings').settings,

      imagemin = require('gulp-imagemin');

gulp.task('images:clear', () => {
  return del(settings.images.dist);
})

gulp.task('images:build', () => {

  return gulp.src([ settings.images.src + '**/*.+(png|jpg|jpeg|gif|svg|ico)' ],
    { since: gulp.lastRun('images:build')
  })

  .pipe(plumber(function (error) {
    util.log(util.colors.red('Error (' + error.plugin + '): ' + error.message));
    this.emit('end');
  }))
  .pipe(imagemin())
  .pipe(gulp.dest(settings.images.dist))
  .pipe(browserSync.stream());
});

gulp.task('images', gulp.series('images:clear', 'images:build'))
