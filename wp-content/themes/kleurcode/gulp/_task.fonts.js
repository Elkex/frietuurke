const gulp = require('gulp'),
      del = require('del'),
      util = require('gulp-util'),
      plumber = require('gulp-plumber'),
      browserSync = require('browser-sync').get('Dev Server'),

      settings = require('./_settings').settings;

gulp.task('fonts:clear', function () {
    return del(settings.fonts.dist);
});

gulp.task('fonts:build', function () {
    return gulp.src([
      settings.fonts.src + '**/*.{ttf,otf,eot,svg,woff,woff2}'],
      { since: gulp.lastRun('fonts:build') })
    .pipe(plumber(function (error) {
      util.log(util.colors.red('Error (' + error.plugin + '): ' + error.message));
      this.emit('end');
    }))
    .pipe(gulp.dest(settings.fonts.dist))
    .pipe(browserSync.stream());
});

gulp.task('fonts', gulp.series('fonts:clear', 'fonts:build'))
