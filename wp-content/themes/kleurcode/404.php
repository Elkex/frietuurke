<?php get_template_part('partials/head'); ?>

<?php get_template_part('partials/header'); ?>

<main class="c-main" role="main">
    <section>
        <div class="g-container">
            <div class="g-flex g-flex-column g-flex-justify-content-center g-flex-align-items-center">
                <img src="https://kleurcode.be/wp-content/uploads/2020/06/kleurcode_logo_outline_grey.gif" title="Kleurcode 404 logo outline" alt="Kleurcode 404 logo outline" />
                <h2 class="e-text-center g-m-y-lg">Oeps! Alle kleur lijkt even verdwenen!</h2>
                <p class="e-text-center">
                    De pagina die je zoekt blijkt niet te bestaan. We helpen je meteen weer op weg!
                </p>
            </div>
        </div>
    </section>


    <section class="l-portfolio-wrapper l-portfolio g-p-y-xxl">
        <div class="g-container">
            <h3 class="e-text-center g-m-bottom-lg">
                Bekijk wie we ondertussen reeds kleur gegeven hebben
            </h3>
            <div class="js-masonry g-flex g-flex-wrap">
                <?php

                $args = array(
                    'posts_per_page' => 8,
                    'post_type' => 'portfolio',
                    'post_status' => 'publish',
                    'orderby' => 'rand',
                );

                $loop = new WP_Query($args);

                while ($loop->have_posts()) : $loop->the_post(); ?>
                    <div class="l-portfolio-thumbnail">
                        <div class="l-portfolio-item">
                            <?php echo get_the_post_thumbnail(); ?>
                            <div class="l-portfolio-thumbnail-content">
                                <h4 class="g-m-none has-<?php echo get_post_meta($post->ID, '_thumbnail_text_color_slug', true); ?>-color">
                                    <?php echo the_title(); ?>
                                </h4>
                            </div>
                            <a href="<?php the_permalink(); ?>" title="Ontdek het project <?php the_title(); ?> van Kleurcode" class="l-portfolio-thumbnail-btn"></a>
                        </div>
                    </div>
                <?php endwhile; ?>

                <?php wp_reset_postdata(); ?>
            </div>

            <div class="g-flex g-flex-justify-content-center g-m-y-lg">
                <a href="<?php echo get_the_permalink(56); ?>" class="c-button c-button-primary">
                    Bekijk meer projecten
                </a>
            </div>

        </div>
    </section>
</main>

<?php get_template_part('partials/footer'); ?>