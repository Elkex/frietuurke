<?php
$options = get_option('theme_settings');
?>

<div class="p-nav">
    <div class="p-nav__bandeau">
    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/frietuurke-pattern.svg" alt="Frietuurke pattern" />
    </div>
    <?php /*
    <div class="p-nav__sub">
        <div class="g-container">
            <div class="g-flex g-flex-align-items-center">
                <svg class="c-icon c-icon-phone">
                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#phone"></use>
                </svg>
                <p>
                    Je kan bestellen via:
                    <a href="tel:<?php echo $options['phone']; ?>" target="_blank">
                        <?php echo $options['phone']; ?>
                    </a>
                </p>

            </div>
        </div>
    </div>
    */
    ?>
        <div class="g-container">
        <div class="g-flex g-flex-column g-flex-row-lg g-flex-justify-content-between">
            <div class="p-nav__top g-flex g-flex-justify-content-between g-flex-align-items-center g-flex-grow g-flex-auto-lg g-p-y-md g-p-y-sm-lg">
                <a href="<?php echo get_site_url(); ?>" class="p-nav-logo">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/logo_frietuurke.svg" alt="Logo">
                </a>
                <a href="#" class="p-nav-toggle g-block g-none-xl">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </div>
            <nav class="p-nav-main g-m-top-xl g-m-top-none-xl g-none g-flex-xl g-flex-column g-flex-row-xl g-flex-align-items-center g-flex-justify-content-center">
                <ul class="g-m-none g-flex g-flex-align-items-center g-flex-justify-content-center g-flex-column g-flex-row-xl">
                    <?php
                    //var_dump(wp_get_nav_menu_items('primary'));
                    foreach (get_menu_items('primary') as $item) : ?>
                        <li>
                            <a href="<?php echo $item['url']; ?>" class="p-nav-button-title g-p-top-sm g-p-bottom-md<?php echo $item['id'] == get_queried_object_id() ? ' active' : ''; ?>" title="<?php echo $item['name']; ?>">
                                <?php echo $item['name']; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </nav>
        </div>
    </div>
</div>