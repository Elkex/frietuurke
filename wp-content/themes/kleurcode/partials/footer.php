<?php
$options = get_option('theme_settings');
?>

<footer class="p-footer" role="contentinfo">
    <div class="g-container g-p-y-xl">
        <div class="g-grid">
            <div class="g-col g-col-12 g-col-3-md g-flex g-flex-column g-flex-align-items-center g-flex-align-items-start-lg">
                <a href="/">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/logo_frietuurke.svg" alt="Logo" class="p-footer-logo">
                </a> 
            </div>
            <div class="g-col g-col-12 g-col-3-md g-flex g-flex-column g-flex-align-items-center g-flex-align-items-start-lg">
                <div class="e-text-center e-text-left-lg">
                    <h4 class="g-m-top-lg g-m-top-none-lg">Je kan ons terugvinden</h4>
                    <?php if (!empty($options['adres'])) : ?> 
                        <p> 
                            <?php echo nl2br($options['adres']); ?>
                        </p>
                    <?php endif; ?>
                    <?php if (!empty($options['phone'])) : ?>
                        <a href="tel:<?php echo $options['phone']; ?>" target="_blank" class="p-footer__phone">
                            <div class="g-flex g-flex-align-items-center">
                                <svg class="c-icon c-icon-phone g-m-right-xs">
                                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#phone"></use>
                                </svg>
                                <?php echo $options['phone']; ?>
                            </div>
                        </a>
                    <?php endif; ?>
                    <?php if (!empty($options['hours'])) : ?>
                        <h4 class="g-m-top-md">
                            Openingsuren
                        </h4>
                        <p>
                            <?php echo nl2br($options['hours']); ?>
                        </p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="g-col g-col-12 g-col-3-md g-flex g-flex-column g-flex-align-items-center g-flex-align-items-start-lg">
                <h4 class="g-m-top-lg g-m-top-none-lg">Menu</h4>
                <nav class="p-footer-nav">
                    <ul class="g-flex g-flex-column g-flex-align-items-center g-flex-align-items-start-lg">
                        <?php foreach (get_menu_items('primary') as $item) : ?>
                            <li>
                                <a href="<?php echo $item['url']; ?>" class="p-nav-item">
                                    <?php echo $item['name']; ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </nav>
            </div>
            <div class="g-col g-col-12 g-col-3-md g-flex g-flex-column g-flex-align-items-center g-flex-align-items-start-lg">
                <div>
                    <h4 class="g-m-top-lg g-m-top-none-lg">Volg ons op sociale media</h4>
                    <ul class="g-flex g-flex-align-items-center g-flex-justify-content-center g-flex-justify-content-start-lg">
                        <?php if (!empty($options['facebookurl'])) : ?>
                            <li>
                                <a href="<?php echo $options['facebookurl']; ?>" target="_blank">
                                    <svg class="c-icon c-icon-facebook">
                                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#facebook"></use>
                                    </svg>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (!empty($options['instagramurl'])) : ?>
                            <li class="g-m-left-sm">
                                <a href="<?php echo $options['instagramurl']; ?>" target="_blank">
                                    <svg class="c-icon c-icon-instagram">
                                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#instagram"></use>
                                    </svg>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (!empty($options['twitterurl'])) : ?>
                            <li class="g-m-left-sm">
                                <a href="<?php echo $options['twitterurl']; ?>" target="_blank">
                                    <svg class="c-icon c-icon-twitter">
                                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#twitter"></use>
                                    </svg>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="p-footer-sub g-p-y-xs">
        <div class="g-grid">
            <div class="g-container">
                <div class="g-col g-col-12">
                    <div class="g-flex g-flex-column g-flex-align-items-center g-flex-row-md g-flex-justify-content-between-md">
                        <p>
                            <small>
                                &copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?> | <a href="/privacy-policy">Privacy Policy</a>
                            </small>
                        </p>
                        <p>
                            <small>
                                Webdesign & development by <a href="https://www.elkemoras.be" title="Website Elke Moras">Elke Moras</a>
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<?php echo wp_footer(); ?>

</body>

</html>