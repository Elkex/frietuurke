<?php get_template_part('partials/head'); ?>

<?php get_template_part('partials/nav'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <main class="c-main" role="main">
            <?php the_content(); ?>
        </main>
    <?php endwhile; ?>
<?php endif; ?>

<section class="l-portfolio-wrapper l-portfolio g-p-y-xl">
    <div class="g-container">
        <h2 class="g-m-bottom-xl">-- Bekijk ook onze andere projecten</h2>
        <div class="js-masonry">
            <?php

            $args = array(
                'posts_per_page' => 8,
                'post_type' => 'portfolio',
                'post_status' => 'publish',
                'post__not_in' => array($post->ID),
                'orderby' => 'rand',
            );

            $loop = new WP_Query($args);

            while ($loop->have_posts()) : $loop->the_post(); ?>
                <div class="l-portfolio-thumbnail">
                    <div class="l-portfolio-item">
                        <?php echo get_the_post_thumbnail(); ?>
                        <div class="l-portfolio-thumbnail-content">
                            <h4 class="g-m-none has-<?php echo get_post_meta($post->ID, '_thumbnail_text_color_slug', true); ?>-color">
                                <?php echo the_title(); ?>
                            </h4>
                        </div>
                        <a href="<?php the_permalink(); ?>" title="Ontdek het project <?php the_title(); ?> van Kleurcode" class="l-portfolio-thumbnail-btn"></a>
                    </div>
                </div>
            <?php endwhile; ?>

            <?php wp_reset_postdata(); ?>
        </div>

    </div>
</section>

<section class="has-light-background-color g-p-y-xxl g-m-top-xl">
    <div class="g-container">
        <div class="g-grid">
            <div class="g-col g-col-12">
                <h2 class="e-text-center">
                    Zin om samen te werken?
                </h2>
                <div class="g-flex g-flex-justify-content-center g-m-top-lg">
                    <a href="/contact" class="c-button c-button-primary">
                        Neem contact op
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('partials/footer'); ?>