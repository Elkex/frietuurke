<?php

/**
 * Frontend scripts
 */

function kleurcode_scripts()
{
    wp_enqueue_script('kleurcode_aos_js', get_template_directory_uri() . '/dist/vendor/aos.js', array('jquery'), '');
    wp_enqueue_script('kleurcode_theme_js', get_template_directory_uri() . '/dist/js/main.js', array('jquery'), '1.0.0');
    //wp_enqueue_script('kleurcode_slick', get_template_directory_uri() . '/dist/vendor/slick.min.js', array(), '1.0.0');
}

add_action('wp_enqueue_scripts', 'kleurcode_scripts');