<?php

// Creëer custom post types
function custom_post_type()
{

    // Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x('Portfolio', 'Post Type General Name', 'photographybykimus'),
        'singular_name'       => _x('Portfolio item', 'Post Type Singular Name', 'photographybykimus'),
        'menu_name'           => __('Portfolio', 'photographybykimus'),
        'parent_item_colon'   => __('Parent Portfolio item', 'photographybykimus'),
        'all_items'           => __('All Portfolio', 'photographybykimus'),
        'view_item'           => __('View Portfolio item', 'photographybykimus'),
        'add_new_item'        => __('Add New Portfolio item', 'photographybykimus'),
        'add_new'             => __('Add New', 'photographybykimus'),
        'edit_item'           => __('Edit Portfolio item', 'photographybykimus'),
        'update_item'         => __('Update Portfolio item', 'photographybykimus'),
        'search_items'        => __('Search Portfolio item', 'photographybykimus'),
        'not_found'           => __('Not Found', 'photographybykimus'),
        'not_found_in_trash'  => __('Not found in Trash', 'photographybykimus'),
    );

    // Set other options for Custom Post Type

    $args = array(
        'label'               => __('Portfolio', 'photographybykimus'),
        'description'         => __('Portfolio items', 'photographybykimus'),
        'labels'              => $labels,
        'supports'            => array('title', 'editor', 'excerpt','thumbnail', 'color', 'custom-fields'),
        'hierarchical'        => true,
        'menu_icon' => 'dashicons-camera',
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 6,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'show_in_rest'        => true,
        'single'              => true,
        'taxonomies'          => array('category'),
        'object_subtype'      => 'post',
    );

    // Registering your Custom Post Type
    register_post_type('Portfolio', $args);
}

add_action('init', 'custom_post_type', 0);