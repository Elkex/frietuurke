<?php

/*
** Create Mega Menu
*/

function get_menu_items($nav)
{
    foreach (wp_get_nav_menu_items($nav) as $item) {
        if ($item->menu_item_parent == "0") {
            $mainItemList[] = array('id' => $item->object_id, 'name' => $item->title, 'url' => $item->url);
        };
    };

    if($mainItemList) {

    foreach ($mainItemList as $subItem) :
        foreach (wp_get_nav_menu_items($nav) as $item) :
            if ($subItem['id'] == $item->menu_item_parent) :
                $subItem['children'][] = array('id' => $item->object_id, 'name' => $item->title, 'url' => $item->url, 'parentid' => $item->menu_item_parent);
            else :
                $subItem = $subItem;
            endif;
        endforeach;
        $menu[] = $subItem;
    endforeach;
}
    return $menu;
};