<?php get_template_part('partials/head'); ?>
<?php get_template_part('partials/nav-dark'); ?>

<section class="g-m-y-xxl">
	<div class="g-container">
		<h1>
			<?php echo get_category(get_query_var('cat'))->cat_name; ?>
		</h1>

		<?php
		global $post;
		$category = get_queried_object();
		$cat = $category->term_id;

		$postitems = get_posts(array(
			'post_type' => 'portfolio',
			'posts_per_page' => 5,
			'category'       => $cat,
		));
		?>

		<div class="g-grid">
			<?php foreach ($postitems as $post) : ?>
				<div class="g-col g-col-12 g-col-6-lg">
					<a href="<?php echo get_permalink(); ?>" class="c-card">
						<?php echo get_the_post_thumbnail(); ?>
						<h2 class="g-m-y-lg">
							<?php echo the_title(); ?>
						</h2>
					</a>
				</div>
			<?php endforeach; ?>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
</section>

<?php get_template_part('partials/footer'); ?>