/**
 * A simple Gulp 4 Starter Kit for modern web development.
 *
 * @inpsiration https://github.com/jr-cologne/gulp-starter-kit GitHub Repository
 *
 * ________________________________________________________________________________
 *
 * gulpfile.js
 *
 * The gulp configuration file.
 *
 */

const gulp = require('gulp'),
      del = require('del'),
      browserSync = require('browser-sync').create('Dev Server'),
      settings = require('./gulp/_settings').settings;

require('./gulp/_task.scss.js');

require('./gulp/_task.js.js');

require('./gulp/_task.images.js');

require('./gulp/_task.vendor.js');

require('./gulp/_task.sprite.js');

require('./gulp/_task.fonts.js');

gulp.task('clear', () => del([ './dist/' ]));

gulp.task('build', gulp.series('clear', gulp.parallel('scss' , 'js', 'images', 'sprite', 'fonts', 'vendor')));

gulp.task('serve', () => {
  return browserSync.init({
    server: {
      baseDir: [ 'dist' ]
    },
    port: 3000,
    open: true
  });
});

gulp.task('watch', () => {

  const watchScss = [
    settings.scss.src + '**/*.scss',
  ]

  const watchJs = [
    settings.js.src + '**/*.+js'
  ]

  const watchImages = [
    settings.images.src + '**/*.+(png|jpg|jpeg|gif|svg|ico)'
  ];

  const watchSprite = [
    settings.sprite.src + '**/*svg'
  ];

  const watchFonts = [
    settings.sprite.src + '**/*.{ttf,otf,eot,svg,woff,woff2}'
  ]

  const watchVendor = [
    settings.vendor.src + '**/*.js'
  ];

  // settings.vendor.src.forEach(dependency => {
  //   watchVendor.push(settings.vendor.dist + dependency + '/**/*.*');
  // });

  gulp.watch(watchScss, gulp.series('scss'));
  gulp.watch(watchJs, gulp.series('js'));
  gulp.watch(watchImages, gulp.series('images:build'));
  gulp.watch(watchSprite, gulp.series('sprite'));
  gulp.watch(watchFonts, gulp.series('fonts:build'));
  gulp.watch(watchVendor, gulp.series('vendor:build'));

});

gulp.task('default', gulp.series('build', gulp.parallel('serve', 'watch')));
