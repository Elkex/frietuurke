/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/animation/edit.js":
/*!*******************************!*\
  !*** ./src/animation/edit.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Edit)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

const {
  Fragment
} = wp.element;
const {
  SelectControl,
  TextControl,
  PanelBody,
  Placeholder
} = wp.components;
const {
  RichText,
  InnerBlocks,
  InspectorControls
} = wp.blockEditor;
const MY_TEMPLATE = [['core/paragraph', {}]];
function Edit(props) {
  const {
    className,
    attributes,
    setAttributes
  } = props;
  const {
    animationType
  } = attributes;
  const setAnimationType = newAnimationType => {
    setAttributes({
      animationType: newAnimationType
    });
  };
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(Fragment, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InspectorControls, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(PanelBody, {
    title: "Animatie settings",
    icon: "none",
    initialOpen: true
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(SelectControl, {
    label: 'Kies je animatie',
    value: animationType,
    onChange: setAnimationType,
    options: [{
      value: null,
      label: 'Geen animatie'
    }, {
      value: 'fade-up',
      label: 'fade-up'
    }, {
      value: 'fade-right',
      label: 'fade-right'
    }, {
      value: 'fade-left',
      label: 'fade-left'
    }]
  }))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: className,
    "data-aos": animationType
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InnerBlocks, {
    template: MY_TEMPLATE
  })));
}

/***/ }),

/***/ "./src/animation/index.js":
/*!********************************!*\
  !*** ./src/animation/index.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit */ "./src/animation/edit.js");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./save */ "./src/animation/save.js");
/* harmony import */ var _utils_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/icons */ "./src/utils/icons.js");



const {
  __
} = wp.i18n;
const {
  registerBlockType
} = wp.blocks;
registerBlockType('kleurcode-core/animation', {
  title: 'Animatie container',
  icon: _utils_icons__WEBPACK_IMPORTED_MODULE_2__["default"].kleurcode,
  category: 'kleurcode-core',
  keywords: ['animation', 'animatie'],
  attributes: {
    animationType: {
      type: 'string'
    }
  },
  edit: _edit__WEBPACK_IMPORTED_MODULE_0__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_1__["default"]
});

/***/ }),

/***/ "./src/animation/save.js":
/*!*******************************!*\
  !*** ./src/animation/save.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Save)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

const {
  InnerBlocks
} = wp.blockEditor;
function Save({
  attributes
}) {
  const {
    animationType
  } = attributes;
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("section", {
    className: attributes.className,
    "data-aos": animationType
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InnerBlocks.Content, null));
}

/***/ }),

/***/ "./src/blockstyles/index.js":
/*!**********************************!*\
  !*** ./src/blockstyles/index.js ***!
  \**********************************/
/***/ (() => {

wp.domReady(function () {
  /**
  * UNREGISTER BLOCK STYLES
  */
  wp.blocks.unregisterBlockStyle('core/image', 'rounded');

  /**
  * REGISTER BLOCK STYLES
  */

  wp.blocks.registerBlockStyle('core/image', [{
    name: 'full',
    label: 'Volledige breedte'
  }]);
});

/***/ }),

/***/ "./src/counter/edit.js":
/*!*****************************!*\
  !*** ./src/counter/edit.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Edit)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

const {
  RichText,
  MediaPlaceholder,
  InspectorControls,
  getColorObjectByColorValue
} = wp.blockEditor;
const {
  TextControl,
  PanelBody,
  ColorPalette
} = wp.components;
function Edit(props) {
  const {
    attributes,
    setAttributes,
    className
  } = props;
  const {
    text,
    counter,
    image,
    imgId,
    colorSlug,
    colorHex
  } = attributes;
  let colors = wp.data.select("core/editor").getEditorSettings().colors;
  const updateCounter = newCounter => {
    setAttributes({
      counter: parseInt(newCounter)
    });
  };
  const updateText = newText => {
    setAttributes({
      text: newText
    });
  };
  const updateImage = newImage => {
    setAttributes({
      image: newImage
    });
    setAttributes({
      imgId: newImage
    });
  };
  const updateColor = newColor => {
    let colorClass = getColorObjectByColorValue(wp.data.select('core/editor').getEditorSettings().colors, newColor).slug;
    setAttributes({
      colorHex: newColor
    });
    setAttributes({
      colorSlug: 'has-' + colorClass + '-color'
    });
  };
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: 'c-counter'
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InspectorControls, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(PanelBody, {
    header: "Counter nummer"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(TextControl, {
    type: "number",
    onChange: updateCounter,
    value: counter,
    label: "Typ hier je counter nummer."
  }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(ColorPalette, {
    colors: colors,
    onChange: updateColor,
    value: colorHex
  }))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(MediaPlaceholder, {
    onSelect: updateImage,
    allowedTypes: ['image'],
    multiple: false,
    value: imgId,
    labels: !imgId ? {
      title: 'The Image'
    } : {
      title: '',
      instructions: ''
    }
  }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("figure", {
    className: "g-flex g-flex-align-items-center"
  }, image && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("img", {
    src: image.url,
    className: "c-counter-icon"
  })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", {
    className: 'c-counter-number g-m-none ' + colorSlug
  }, counter), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(RichText, {
    onChange: updateText,
    value: text,
    className: 'c-counter-text ' + colorSlug,
    placeholder: "Typ hier je tekst.",
    tagName: "p",
    formattingControls: ['textColor']
  }));
}

/***/ }),

/***/ "./src/counter/index.js":
/*!******************************!*\
  !*** ./src/counter/index.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit */ "./src/counter/edit.js");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./save */ "./src/counter/save.js");
/* harmony import */ var _utils_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/icons */ "./src/utils/icons.js");



const {
  __
} = wp.i18n;
const {
  registerBlockType
} = wp.blocks;
registerBlockType('kleurcode-core/counter', {
  title: 'Counter',
  icon: _utils_icons__WEBPACK_IMPORTED_MODULE_2__["default"].kleurcode,
  category: 'kleurcode-core',
  keywords: ['counter', 'aftellen'],
  attributes: {
    text: {
      type: 'string'
    },
    counter: {
      type: 'number'
    },
    image: {
      type: 'object'
    },
    imgId: {
      type: 'object'
    },
    colorHex: {
      type: 'string'
    },
    colorSlug: {
      type: 'string'
    }
  },
  edit: _edit__WEBPACK_IMPORTED_MODULE_0__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_1__["default"]
});

/***/ }),

/***/ "./src/counter/save.js":
/*!*****************************!*\
  !*** ./src/counter/save.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Save)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

const {
  RichText
} = wp.blockEditor;
function Save({
  attributes
}) {
  const {
    counter,
    image,
    text,
    colorSlug
  } = attributes;
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: 'c-counter'
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("figure", {
    className: "g-flex g-flex-align-items-center"
  }, image && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("img", {
    src: image.url,
    className: "c-counter-icon"
  })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", {
    className: 'c-counter-placeholder ' + colorSlug
  }, counter), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", {
    className: 'c-counter-number g-m-none ' + colorSlug
  }, "0"), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", {
    className: 'c-counter-text ' + colorSlug
  }, text));
}

/***/ }),

/***/ "./src/faq/edit.js":
/*!*************************!*\
  !*** ./src/faq/edit.js ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Edit)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

const {
  InnerBlocks,
  RichText
} = wp.blockEditor;
const MY_TEMPLATE = [['core/heading'], ['core/paragraph']];
function Edit(props) {
  const {
    className,
    attributes,
    setAttributes
  } = props;
  const {
    title,
    text
  } = attributes;
  const setTitle = newTitle => {
    setAttributes({
      title: newTitle
    });
  };
  const setText = nexText => {
    setAttributes({
      text: nexText
    });
  };
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: className
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(RichText, {
    onChange: setTitle,
    value: title,
    placeholder: "Typ hier je FAQ vraag.",
    tagName: "h4"
  }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(RichText, {
    onChange: setText,
    value: text,
    allowedFormats: ['bold', 'italic'],
    placeholder: "Typ hier je FAQ tekst.",
    tagName: "p"
  }));
}

/***/ }),

/***/ "./src/faq/index.js":
/*!**************************!*\
  !*** ./src/faq/index.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit */ "./src/faq/edit.js");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./save */ "./src/faq/save.js");
/* harmony import */ var _utils_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/icons */ "./src/utils/icons.js");



const {
  __
} = wp.i18n;
const {
  registerBlockType
} = wp.blocks;
registerBlockType('kleurcode-core/faq', {
  title: 'FAQ item',
  icon: _utils_icons__WEBPACK_IMPORTED_MODULE_2__["default"].kleurcode,
  category: 'kleurcode-core',
  keywords: ['faq', 'vraag'],
  attributes: {
    title: {
      type: 'string'
    },
    text: {
      type: 'string'
    }
  },
  edit: _edit__WEBPACK_IMPORTED_MODULE_0__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_1__["default"]
});

/***/ }),

/***/ "./src/faq/save.js":
/*!*************************!*\
  !*** ./src/faq/save.js ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Save)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

/**
 * Voeg FAQ item toe
 */

// Import block dependencies.

function Save({
  attributes
}) {
  const {
    title,
    text
  } = attributes;
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("section", {
    className: attributes.className
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "c-faq-item"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "c-faq-item-title"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("h4", {
    className: "g-flex g-flex-justify-content-between g-flex-align-content-center"
  }, title, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("svg", {
    className: "c-icon c-icon-chevron-down g-m-right-xs",
    xmlns: "http://www.w3.org/2000/svg",
    id: "chevron-down",
    viewBox: "0 0 16 9.9",
    xmlSpace: "preserve"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("g", {
    id: "cdown-arrow",
    transform: "translate(0 -48.907)"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("g", {
    id: "cGroup_3",
    transform: "translate(0 48.907)"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("path", {
    id: "cPath_24",
    className: "cst0",
    d: "M14.1 0L8 6.1 1.9 0 0 1.9l8 8 8-8L14.1 0z"
  })))))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "c-faq-item-content"
  }, text)));
}
;

/***/ }),

/***/ "./src/header/edit.js":
/*!****************************!*\
  !*** ./src/header/edit.js ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Edit)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

// Import block dependencies.

const {
  InnerBlocks,
  MediaPlaceholder,
  InspectorControls,
  getColorObjectByColorValue
} = wp.blockEditor;
const {
  PanelBody,
  SelectControl,
  ColorPalette,
  ToggleControl
} = wp.components;
function Edit(props) {
  const {
    attributes,
    setAttributes,
    className
  } = props;
  const {
    image,
    height,
    negMargin,
    colorHexa,
    colorSlug,
    imgId,
    opacity
  } = attributes;
  let colors = wp.data.select('core/editor').getEditorSettings().colors;
  const updateImage = newImage => {
    setAttributes({
      image: newImage
    });
    setAttributes({
      imgId: newImage
    });
  };
  const updateHeight = newHeight => {
    setAttributes({
      height: newHeight
    });
  };
  const updateMargin = newMargin => {
    setAttributes({
      negMargin: newMargin
    });
  };
  const updateColor = newColor => {
    let colorClass = getColorObjectByColorValue(wp.data.select('core/editor').getEditorSettings().colors, newColor).slug;
    setAttributes({
      colorHexa: newColor
    });
    setAttributes({
      colorSlug: colorClass
    });
  };
  const updateOpacity = newOpacity => {
    setAttributes({
      opacity: newOpacity
    });
  };
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: className
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(MediaPlaceholder, {
    onSelect: updateImage,
    allowedTypes: ['image'],
    multiple: false,
    value: imgId,
    labels: {
      title: 'The Image'
    }
  }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("header", {
    className: `p-header p-header-wrapper g-p-y-xxl ` + height + ' p-header-' + colorSlug + (opacity ? ' p-header-opacity' : '')
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("figure", {
    className: "g-flex g-flex-align-items-center"
  }, image && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("img", {
    src: image.url,
    className: "p-header-img"
  }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("figcaption", {
    className: "p-header-content"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "g-container"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InnerBlocks, null))))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InspectorControls, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(PanelBody, {
    title: "Header"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(ColorPalette, {
    colors: colors,
    onChange: updateColor,
    value: colorHexa
  }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(SelectControl, {
    label: 'Header hoogte',
    help: 'Bepaal de hoogte van je header',
    value: !height ? setAttributes({
      height: 'p-header-wrapper-m'
    }) : height,
    onChange: updateHeight,
    options: [{
      label: 'Kies hoogte',
      value: null,
      disabled: true
    }, {
      label: 'Geen',
      value: ''
    }, {
      label: '200 px',
      value: 'p-header-wrapper-xs'
    }, {
      label: '300 px',
      value: 'p-header-wrapper-s'
    }, {
      label: '400 px',
      value: 'p-header-wrapper-m'
    }, {
      label: '600 px',
      value: 'p-header-wrapper-l'
    }]
  }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(SelectControl, {
    label: 'Header hoogte',
    help: 'Bepaal de hoogte van je header',
    value: !negMargin ? setAttributes({
      negMargin: ''
    }) : negMargin,
    onChange: updateMargin,
    options: [{
      label: 'Geen',
      value: ''
    }, {
      label: 'Negatieve marge',
      value: 'p-header-neg'
    }]
  }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(ToggleControl, {
    label: "Transparante achtergrond",
    help: opacity ? 'De achtergrond is transparant.' : 'De achtergrond is niet transparant.',
    checked: opacity,
    onChange: updateOpacity
  }))));
}

/***/ }),

/***/ "./src/header/index.js":
/*!*****************************!*\
  !*** ./src/header/index.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit */ "./src/header/edit.js");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./save */ "./src/header/save.js");
/* harmony import */ var _utils_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/icons */ "./src/utils/icons.js");



const {
  __
} = wp.i18n;
const {
  registerBlockType
} = wp.blocks;
registerBlockType('kleurcode-core/header', {
  title: 'Header',
  icon: _utils_icons__WEBPACK_IMPORTED_MODULE_2__["default"].kleurcode,
  category: 'kleurcode-core',
  keywords: ['header', 'hero'],
  attributes: {
    image: {
      type: 'object'
    },
    height: {
      type: 'string'
    },
    negMargin: {
      type: 'string'
    },
    colorHexa: {
      type: 'string'
    },
    colorSlug: {
      type: 'string'
    },
    imgId: {
      type: 'object'
    },
    opacity: {
      type: 'boolean'
    }
  },
  edit: _edit__WEBPACK_IMPORTED_MODULE_0__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_1__["default"]
});

/***/ }),

/***/ "./src/header/save.js":
/*!****************************!*\
  !*** ./src/header/save.js ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Save)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

/**
 * Voeg Header toe
 */

// Import block dependencies.

const {
  InnerBlocks
} = wp.blockEditor;
function Save({
  attributes
}) {
  const {
    image,
    height,
    negMargin,
    colorSlug,
    opacity
  } = attributes;
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("header", {
    className: 'p-header p-header-wrapper g-p-y-xl ' + height + ' ' + negMargin + ' p-header-' + colorSlug + (opacity ? ' p-header-opacity' : '')
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("figure", {
    className: "g-flex g-flex-align-items-center"
  }, image && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("img", {
    src: image.url,
    className: "p-header-img"
  }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("figcaption", {
    className: "p-header-content"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "g-container"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InnerBlocks.Content, null)))));
}
;

/***/ }),

/***/ "./src/lists/index.js":
/*!****************************!*\
  !*** ./src/lists/index.js ***!
  \****************************/
/***/ (() => {

wp.domReady(() => {
  wp.blocks.unregisterBlockStyle('core/list', ['checklist']);
  wp.blocks.registerBlockStyle('core/list', [{
    name: 'checklist',
    label: 'Checklist',
    isDefault: true
  }, {
    name: 'plus',
    label: 'Plus',
    isDefault: false
  }]);
});

/***/ }),

/***/ "./src/parallax/edit.js":
/*!******************************!*\
  !*** ./src/parallax/edit.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Edit)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

// Import block dependencies.

const {
  InnerBlocks,
  MediaPlaceholder,
  InspectorControls,
  getColorObjectByColorValue
} = wp.blockEditor;
const {
  PanelBody,
  SelectControl,
  ColorPalette,
  ToggleControl
} = wp.components;
function Edit(props) {
  const {
    attributes,
    setAttributes,
    className
  } = props;
  const {
    image,
    height,
    imgId
  } = attributes;
  let colors = wp.data.select('core/editor').getEditorSettings().colors;
  const updateImage = newImage => {
    setAttributes({
      image: newImage
    });
    setAttributes({
      imgId: newImage
    });
  };
  const updateHeight = newHeight => {
    setAttributes({
      height: newHeight
    });
  };
  let divStyle;
  if (image) {
    let imgUrl = image.url;
    divStyle = {
      backgroundImage: "url(" + imgUrl + ")"
    };
  }
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: className
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(MediaPlaceholder, {
    onSelect: updateImage,
    allowedTypes: ['image'],
    multiple: false,
    value: imgId,
    labels: {
      title: 'The Image'
    }
  }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    style: divStyle,
    className: `c-parallax c-parallax-wrapper g-p-y-xxl ` + height
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "g-container"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InnerBlocks, null))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InspectorControls, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(PanelBody, {
    title: "Parallax"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(SelectControl, {
    label: 'Parallax hoogte',
    help: 'Bepaal de hoogte van je parallax',
    value: !height ? setAttributes({
      height: 'c-parallax-wrapper-m'
    }) : height,
    onChange: updateHeight,
    options: [{
      label: 'Kies hoogte',
      value: null,
      disabled: true
    }, {
      label: 'Geen',
      value: ''
    }, {
      label: '200 px',
      value: 'c-parallax-wrapper-xs'
    }, {
      label: '300 px',
      value: 'c-parallax-wrapper-s'
    }, {
      label: '400 px',
      value: 'c-parallax-wrapper-m'
    }, {
      label: '600 px',
      value: 'c-parallax-wrapper-l'
    }]
  }))));
}

/***/ }),

/***/ "./src/parallax/index.js":
/*!*******************************!*\
  !*** ./src/parallax/index.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit */ "./src/parallax/edit.js");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./save */ "./src/parallax/save.js");
/* harmony import */ var _utils_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/icons */ "./src/utils/icons.js");



const {
  __
} = wp.i18n;
const {
  registerBlockType
} = wp.blocks;
registerBlockType('kleurcode-core/parallax', {
  title: 'Parallax',
  icon: _utils_icons__WEBPACK_IMPORTED_MODULE_2__["default"].kleurcode,
  category: 'kleurcode-core',
  keywords: ['parallax'],
  attributes: {
    image: {
      type: 'object'
    },
    height: {
      type: 'string'
    },
    imgId: {
      type: 'object'
    }
  },
  edit: _edit__WEBPACK_IMPORTED_MODULE_0__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_1__["default"]
});

/***/ }),

/***/ "./src/parallax/save.js":
/*!******************************!*\
  !*** ./src/parallax/save.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Save)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

/**
 * Voeg Header toe
 */

// Import block dependencies.

const {
  InnerBlocks
} = wp.blockEditor;
function Save({
  attributes
}) {
  const {
    image,
    height
  } = attributes;
  let divStyle;
  if (image) {
    let imgUrl = image.url;
    divStyle = {
      backgroundImage: "url(" + imgUrl + ")"
    };
  }
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    style: divStyle,
    className: 'c-parallax c-parallax-wrapper g-p-y-xxl ' + height
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "g-container"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InnerBlocks.Content, null)));
}
;

/***/ }),

/***/ "./src/slideshow/edit.js":
/*!*******************************!*\
  !*** ./src/slideshow/edit.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

// Import block dependencies.

const {
  compose
} = wp.compose;
const {
  withSelect,
  select
} = wp.data;
const {
  InnerBlocks
} = wp.blockEditor;
const {
  Component
} = wp.element;
const MY_TEMPLATE = [['core/gallery']];
function Edit(props) {
  const {
    attributes,
    setAttributes,
    className,
    changedImages
  } = props;
  const {
    images
  } = attributes;
  if (changedImages && changedImages.length > 0) {
    setAttributes({
      images: changedImages
    });
  }
  console.log(images);
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: className
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InnerBlocks, {
    template: MY_TEMPLATE,
    templateLock: "all"
  }));
}
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (compose([withSelect((select, block) => {
  let changedImages = [];
  const selectedBlock = select('core/block-editor').getBlocksByClientId(select('core/block-editor').getSelectedBlockClientId());
  const parentBlock = select('core/block-editor').getBlocksByClientId(select('core/block-editor').getBlockHierarchyRootClientId(select('core/block-editor').getSelectedBlockClientId()));
  if (selectedBlock[0]) {
    if (selectedBlock[0].name === "core/gallery" && parentBlock[0].name === "kleurcode-core/slideshow") {
      if (document.querySelector(".components-panel__body") !== null) {
        document.querySelector(".components-panel__body").style.display = "none";
        document.querySelector(".components-panel__body").style.visibility = "hidden";
        document.querySelector(".components-panel__body").style.opacity = "0";
      }
      if (document.querySelector(".is-opened") !== null) {
        document.querySelector(".is-opened").style.display = "none";
        document.querySelector(".is-opened").style.visibility = "hidden";
        document.querySelector(".is-opened").style.opacity = "0";
      }
      if (document.querySelector(".components-popover") !== null) {
        document.querySelector(".components-popover").style.display = "none";
        document.querySelector(".components-popover").style.visibility = "hidden";
        document.querySelector(".components-popover").style.opacity = "0";
      }
    }
  }
  const parentBlockimg = select('core/block-editor').getBlocksByClientId(block.clientId)[0].innerBlocks[0];
  if (parentBlockimg) {
    changedImages = parentBlockimg.attributes.images;
  }
  return {
    changedImages
  };
})])(Edit));

/***/ }),

/***/ "./src/slideshow/index.js":
/*!********************************!*\
  !*** ./src/slideshow/index.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit */ "./src/slideshow/edit.js");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./save */ "./src/slideshow/save.js");
/* harmony import */ var _utils_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/icons */ "./src/utils/icons.js");



const {
  __
} = wp.i18n;
const {
  registerBlockType
} = wp.blocks;
registerBlockType('kleurcode-core/slideshow', {
  title: 'Slideshow',
  icon: _utils_icons__WEBPACK_IMPORTED_MODULE_2__["default"].kleurcode,
  category: 'kleurcode-core',
  keywords: ['afbeelding', 'gallerij', 'slideshow'],
  attributes: {
    images: {
      type: 'array'
    }
  },
  edit: _edit__WEBPACK_IMPORTED_MODULE_0__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_1__["default"]
});

/***/ }),

/***/ "./src/slideshow/save.js":
/*!*******************************!*\
  !*** ./src/slideshow/save.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Save)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

/**
 * Voeg FAQ item toe
 */

// Import block dependencies.

const {
  InnerBlocks
} = wp.blockEditor;
function Save({
  attributes
}) {
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("section", null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InnerBlocks.Content, null));
}
;

/***/ }),

/***/ "./src/tab/edit.js":
/*!*************************!*\
  !*** ./src/tab/edit.js ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Edit)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

const {
  Fragment
} = wp.element;
const {
  InnerBlocks,
  InspectorControls
} = wp.blockEditor;
const {
  PanelBody,
  PanelRow,
  TextControl
} = wp.components;
function Edit(props) {
  const {
    attributes,
    setAttributes,
    className,
    clientId
  } = props;
  const {
    tabTitle,
    tabId
  } = attributes;
  const updateTabTitle = newTabTitle => {
    setAttributes({
      tabTitle: newTabTitle
    });
  };
  setAttributes({
    tabId: clientId
  });
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(Fragment, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InspectorControls, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(PanelBody, {
    title: "Voer tab titel in",
    icon: "none",
    initialOpen: true
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(PanelRow, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(TextControl, {
    label: "Tab titel",
    value: tabTitle,
    onChange: updateTabTitle
  })))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: attributes.className
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InnerBlocks, null)));
}

/***/ }),

/***/ "./src/tab/index.js":
/*!**************************!*\
  !*** ./src/tab/index.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit */ "./src/tab/edit.js");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./save */ "./src/tab/save.js");
/* harmony import */ var _utils_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/icons */ "./src/utils/icons.js");



const {
  registerBlockType
} = wp.blocks;
registerBlockType('kleurcode-core/tab', {
  title: 'Tab',
  icon: _utils_icons__WEBPACK_IMPORTED_MODULE_2__["default"].kleurcode,
  category: 'kleurcode-core',
  parent: ['kleurcode-core/tabs'],
  keywords: ['tabs', 'tab'],
  supports: {
    html: false
  },
  attributes: {
    tabTitle: {
      type: 'string'
    },
    tabId: {
      type: 'string'
    }
  },
  edit: _edit__WEBPACK_IMPORTED_MODULE_0__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_1__["default"]
});

/***/ }),

/***/ "./src/tab/save.js":
/*!*************************!*\
  !*** ./src/tab/save.js ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Save)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

const {
  InnerBlocks
} = wp.blockEditor;
function Save({
  attributes
}) {
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("section", {
    id: attributes.tabId,
    className: (attributes.className, 'g-p-md')
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InnerBlocks.Content, null));
}

/***/ }),

/***/ "./src/tabs/edit.js":
/*!**************************!*\
  !*** ./src/tabs/edit.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Edit)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

const {
  Fragment,
  useEffect
} = wp.element;
const {
  InnerBlocks
} = wp.blockEditor;
const {
  Placeholder
} = wp.components;
const {
  withSelect,
  select,
  withDispatch
} = wp.data;
const ALLOWED_BLOCKS = ['kleurcode-core/tab'];
function Edit(props) {
  const {
    attributes,
    setAttributes,
    className,
    clientId
  } = props;
  const {
    tabItems
  } = attributes;
  const parentBlock = select('core/block-editor').getBlocksByClientId(clientId)[0];
  const tabs = parentBlock.innerBlocks;
  const clickTab = e => {
    e.preventDefault();
    if (document.querySelectorAll("div[data-type='kleurcode-core/tab'").length > 0) {
      const allTabs = document.querySelectorAll("div[data-type='kleurcode-core/tab'");
      [...allTabs].forEach(allTab => allTab.style.display = 'none');
    }
    document.getElementById('block-' + e.target.getAttribute('data-tab')).style.display = 'block';
  };
  useEffect(() => {
    updateTabs();
  }, [tabs]);
  const updateTabs = () => {
    let tabsObj = [];
    if (tabs && tabs.length > 0) {
      tabs.map((tab, index) => {
        tabsObj.push({
          'id': tab.clientId,
          'title': tab.attributes.tabTitle
        });
      });
    }
    setAttributes({
      tabItems: tabsObj
    });
  };
  if (tabItems.length < 1) {
    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(Placeholder, {
      key: "placeholder",
      icon: "editor-table",
      label: "Tab layout",
      instructions: "Kies je tabs"
    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InnerBlocks, {
      allowedBlocks: ALLOWED_BLOCKS
    })));
  }
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(Fragment, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: className
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("nav", {
    className: "c-tab-nav o-flex"
  }, tabItems && tabItems.map((tab, index) => {
    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("a", {
      href: "#",
      className: "c-tab-nav-item c-button",
      onClick: clickTab,
      "data-tab": tab.id
    }, tab.title);
  })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    class: "c-tab-content"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InnerBlocks, {
    allowedBlocks: ALLOWED_BLOCKS
  }))));
}

/***/ }),

/***/ "./src/tabs/index.js":
/*!***************************!*\
  !*** ./src/tabs/index.js ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit */ "./src/tabs/edit.js");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./save */ "./src/tabs/save.js");
/* harmony import */ var _utils_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/icons */ "./src/utils/icons.js");



const {
  registerBlockType
} = wp.blocks;
registerBlockType('kleurcode-core/tabs', {
  title: 'Tabs',
  icon: _utils_icons__WEBPACK_IMPORTED_MODULE_2__["default"].kleurcode,
  category: 'kleurcode-core',
  keywords: ['tabs', 'tab'],
  supports: {
    html: false
  },
  attributes: {
    tabItems: {
      type: 'array',
      default: []
    }
  },
  edit: _edit__WEBPACK_IMPORTED_MODULE_0__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_1__["default"]
});

/***/ }),

/***/ "./src/tabs/save.js":
/*!**************************!*\
  !*** ./src/tabs/save.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Save)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

const {
  InnerBlocks
} = wp.blockEditor;
function Save({
  attributes
}) {
  const {
    tabItems
  } = attributes;
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("section", {
    className: (attributes.className, 'c-tabs')
  }, tabItems.length > 0 && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("nav", {
    className: "c-tab-nav"
  }, tabItems.map((tab, index) => {
    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("a", {
      href: "#",
      className: "c-tab-nav-item",
      "data-tab": tab.id
    }, tab.title);
  })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    class: "c-tab-content"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InnerBlocks.Content, null)));
}

/***/ }),

/***/ "./src/thumbnail-text/index.js":
/*!*************************************!*\
  !*** ./src/thumbnail-text/index.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

/**
 * Post Intro Type.
 */

const {
  registerPlugin
} = wp.plugins;
const {
  PluginPostStatusInfo,
  PluginDocumentSettingPanel
} = wp.editPost;
const {
  Fragment,
  useState
} = wp.element;
const {
  Button,
  ColorPalette
} = wp.components;
const {
  useEntityProp
} = wp.coreData;
const {
  select,
  useSelect
} = wp.data;
const {
  getColorObjectByColorValue
} = wp.blockEditor;
const ctaText = props => {
  const {
    attributes
  } = props;
  const postType = useSelect(select => select('core/editor').getCurrentPostType(), []);
  const [meta, setMeta] = useEntityProp('postType', postType, 'meta');
  const [thumbnailTextColorHex, setThumbnailTextColorHex] = useState(meta['_thumbnail_text_color_hex'] || '');
  let colors = wp.data.select("core/editor").getEditorSettings().colors;
  const updateThumbnailTextColor = newThumbnailTextColor => {
    let colorClass = getColorObjectByColorValue(wp.data.select('core/editor').getEditorSettings().colors, newThumbnailTextColor).slug;
    setThumbnailTextColorHex(newThumbnailTextColor);
    setMeta({
      ...meta,
      '_thumbnail_text_color_hex': newThumbnailTextColor,
      '_thumbnail_text_color_slug': colorClass
    });
  };
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(Fragment, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(PluginDocumentSettingPanel, {
    className: "klasse-sidebar-thumbnail-text-toggle",
    name: "thumbnail-text-panel",
    title: "Thumbnail tekst",
    initialOpen: true,
    icon: "none"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(ColorPalette, {
    colors: colors,
    onChange: updateThumbnailTextColor,
    value: thumbnailTextColorHex
  })));
};
registerPlugin('cta-text', {
  render: ctaText
});

/***/ }),

/***/ "./src/unregistered-blocks/index.js":
/*!******************************************!*\
  !*** ./src/unregistered-blocks/index.js ***!
  \******************************************/
/***/ (() => {



/***/ }),

/***/ "./src/utils/icons.js":
/*!****************************!*\
  !*** ./src/utils/icons.js ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

/**
 * Custom icons voor Kleurcode.
 */

const icons = {};

/* Kleurcode logo */
icons.kleurcode = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  role: "img",
  viewBox: "0 0 261.8 242.9"
}, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("g", {
  id: "Layer_1"
}, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("path", {
  class: "st0",
  d: "M113.3 104.5v25.2l25.2-25.2zm34.5 34.6v-25.3l-25.3 25.3z"
}), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("path", {
  class: "st0",
  d: "M244.9 49.4h-24.4l5.2-29.3C227.7 9 220.2 0 209.1 0c-11.1 0-21.7 9-23.7 20.1l-5.2 29.3h-73.4l5.2-29.3C114 9 106.6 0 95.5 0S73.8 9 71.8 20.1l-5.2 29.3H42.3c-11.1 0-21.7 9-23.7 20.1-2 11.1 5.5 20.1 16.6 20.1h24.4L48.4 153H24c-11.1 0-21.7 9-23.7 20.1-2 11.1 5.5 20.1 16.6 20.1h24.4l-5.2 29.3c-2 11.1 5.5 20.1 16.6 20.1 11.1 0 21.7-9 23.7-20.1l5.2-29.3H155l-5.2 29.3c-2 11.1 5.5 20.1 16.6 20.1 11.1 0 21.7-9 23.7-20.1l5.2-29.3h24.4c11.1 0 21.7-9 23.7-20.1 2-11.1-5.5-20.1-16.6-20.1h-24.4l11.2-63.4H238c11.1 0 21.7-9 23.7-20.1 1.7-11-5.7-20.1-16.8-20.1zm-63.8 105c0 1.1-.9 2-2 2h-14v13.2c0 1.1-.9 2-2 2h-13.3c-1.1 0-2-.9-2-2v-13.2h-43.2c-4.8 0-8.7-3.9-8.7-8.7v-43.3H82.7c-1.1 0-2-.9-2-2V89.2c0-1.1.9-2 2-2H96v-14c0-1.1.9-2 2-2h13.3c1.1 0 2 .9 2 2v13.9h42.5l14.9-14.9c.4-.4.9-.6 1.4-.6.5 0 1.1.2 1.4.6l6.4 6.5c.8.8.8 2.1 0 2.9L165 96.5v42.7h14c1.1 0 2 .9 2 2v13.2z"
})));
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (icons);

/***/ }),

/***/ "@wordpress/element":
/*!*********************************!*\
  !*** external ["wp","element"] ***!
  \*********************************/
/***/ ((module) => {

"use strict";
module.exports = window["wp"]["element"];

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _blockstyles_index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./blockstyles/index.js */ "./src/blockstyles/index.js");
/* harmony import */ var _blockstyles_index_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_blockstyles_index_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _lists_index_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./lists/index.js */ "./src/lists/index.js");
/* harmony import */ var _lists_index_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_lists_index_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _unregistered_blocks_index_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./unregistered-blocks/index.js */ "./src/unregistered-blocks/index.js");
/* harmony import */ var _unregistered_blocks_index_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_unregistered_blocks_index_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _animation_index_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./animation/index.js */ "./src/animation/index.js");
/* harmony import */ var _counter_index_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./counter/index.js */ "./src/counter/index.js");
/* harmony import */ var _faq_index_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./faq/index.js */ "./src/faq/index.js");
/* harmony import */ var _header_index_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./header/index.js */ "./src/header/index.js");
/* harmony import */ var _parallax_index_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./parallax/index.js */ "./src/parallax/index.js");
/* harmony import */ var _slideshow_index_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./slideshow/index.js */ "./src/slideshow/index.js");
/* harmony import */ var _tab_index_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./tab/index.js */ "./src/tab/index.js");
/* harmony import */ var _tabs_index_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./tabs/index.js */ "./src/tabs/index.js");
/* harmony import */ var _thumbnail_text_index_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./thumbnail-text/index.js */ "./src/thumbnail-text/index.js");
/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

// CORE BLOCKS





// CUSTOM BLOCKS





//import './recent-cases/index.js';
//import './select-cases/index.js';




})();

/******/ })()
;
//# sourceMappingURL=index.js.map