import edit from './edit';
import save from './save';
import icons from '../utils/icons';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'kleurcode-core/animation', {
	title: 'Animatie container',
	icon: icons.kleurcode,
	category: 'kleurcode-core',
	keywords: [
		'animation','animatie',
	],
	attributes: {
		animationType: {
			type: 'string'
		} 
	},
	edit,
	save
} );
