const {
    Fragment
} = wp.element;

const {
    SelectControl,
    TextControl,
    PanelBody,
    Placeholder,
} = wp.components;

const {
    RichText,
    InnerBlocks,
    InspectorControls
} = wp.blockEditor;

const MY_TEMPLATE = [
    [ 'core/paragraph', {} ],
];

export default function Edit ( props ) {
    const {
        className,
        attributes,
        setAttributes
    } = props;

    const {
        animationType
    } = attributes;

    const setAnimationType = ( newAnimationType ) => {
        setAttributes( {
            animationType: newAnimationType
        } )
    }

    return (
        <Fragment>
            <InspectorControls>
                <PanelBody title="Animatie settings" icon='none' initialOpen={ true }>
                    <SelectControl
                        label={ 'Kies je animatie' }
                        value={ animationType }
                        onChange={ setAnimationType }
                        options={ [
                            {
                                value: null, label: 'Geen animatie'
                            },
                            {
                                value: 'fade-up', label: 'fade-up'
                            },
                            {
                                value: 'fade-right', label: 'fade-right'
                            },
                            {
                                value: 'fade-left', label: 'fade-left'
                            }
                        ] }
                    />
                </PanelBody>
            </InspectorControls>
            <div className={ className } data-aos={ animationType }>
                <InnerBlocks template={MY_TEMPLATE} />
            </div>
        </Fragment>
    )
}