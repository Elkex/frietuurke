const {
    InnerBlocks,
} = wp.blockEditor;

export default function Save ( { attributes } ) {

    const {
        animationType
    } = attributes;

    return (
        <section className={ attributes.className } data-aos={animationType}>
            <InnerBlocks.Content />
        </section>
    )
}