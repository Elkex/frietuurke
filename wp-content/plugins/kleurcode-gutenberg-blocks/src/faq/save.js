/**
 * Voeg FAQ item toe
 */

// Import block dependencies.

export default function Save({ attributes }) {

	const {
		title,
		text
	} = attributes;

	return (
		<section className={attributes.className}>
			<div className="c-faq-item">
				<div className="c-faq-item-title">
					<h4 className="g-flex g-flex-justify-content-between g-flex-align-content-center">
						{title}
						<svg className="c-icon c-icon-chevron-down g-m-right-xs" xmlns="http://www.w3.org/2000/svg" id="chevron-down" viewBox="0 0 16 9.9" xmlSpace="preserve">
							<g id="cdown-arrow" transform="translate(0 -48.907)"><g id="cGroup_3" transform="translate(0 48.907)">
								<path id="cPath_24" className="cst0" d="M14.1 0L8 6.1 1.9 0 0 1.9l8 8 8-8L14.1 0z">
								</path>
							</g>
							</g>
						</svg>
					</h4>
				</div>
				<div className="c-faq-item-content">
					{text}
				</div>
			</div>
		</section>
	);
};