import edit from './edit';
import save from './save';
import icons from '../utils/icons';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'kleurcode-core/faq', {
	title: 'FAQ item',
	icon: icons.kleurcode,
	category: 'kleurcode-core',
	keywords: [
		'faq','vraag',
	],
	attributes: {
		title: {
			type: 'string'
		},
		text: {
			type: 'string'
		}
	},
	edit,
	save
} );
