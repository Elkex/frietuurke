const {
    InnerBlocks,
    RichText
} = wp.blockEditor;

const MY_TEMPLATE = [
    ['core/heading'],
    ['core/paragraph'],
];

export default function Edit(props) {

    const {
        className,
        attributes,
        setAttributes
    } = props;

    const {
        title,
        text
    } = attributes;

    const setTitle = (newTitle) => {
        setAttributes({
            title: newTitle,
        });
    }

    const setText = (nexText) => {
        setAttributes({
            text: nexText,
        });
    }

    return (
        <div className={className}>
            <RichText
                onChange={setTitle}
                value={title}
                placeholder="Typ hier je FAQ vraag."
                tagName="h4"
            />
            <RichText
                onChange={setText}
                value={text}
                allowedFormats={['bold', 'italic']}
                placeholder="Typ hier je FAQ tekst."
                tagName="p"
            />
        </div>
    )
}