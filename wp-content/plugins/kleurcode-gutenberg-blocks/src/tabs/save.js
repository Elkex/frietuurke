const {
    InnerBlocks,
} = wp.blockEditor;

export default function Save({ attributes }) {
    const {
        tabItems
    } = attributes
    return (
        <section className={attributes.className, 'c-tabs'}>
            {tabItems.length > 0 &&
                <nav className="c-tab-nav">
                    {tabItems.map((tab, index) => {
                        return (
                            <a href="#" className="c-tab-nav-item" data-tab={tab.id}>
                                {tab.title}
                            </a>
                        )
                    })
                    }
                </nav>
            }
            <div class="c-tab-content">
                <InnerBlocks.Content />
            </div>
        </section>
    )
}