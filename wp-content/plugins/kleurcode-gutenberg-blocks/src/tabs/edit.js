const {
    Fragment,
    useEffect,
} = wp.element;

const {
    InnerBlocks,
} = wp.blockEditor;

const {
    Placeholder
} = wp.components;

const {
    withSelect,
    select,
    withDispatch
} = wp.data;

const ALLOWED_BLOCKS = ['kleurcode-core/tab'];

export default function Edit(props) {
    const {
        attributes,
        setAttributes,
        className,
        clientId,
    } = props;

    const {
        tabItems
    } = attributes;

    const parentBlock = select('core/block-editor').getBlocksByClientId(clientId)[0];
    const tabs = parentBlock.innerBlocks;

    const clickTab = (e) => {
        e.preventDefault();
        if (document.querySelectorAll("div[data-type='kleurcode-core/tab'").length > 0) {
            const allTabs = document.querySelectorAll("div[data-type='kleurcode-core/tab'");
            [...allTabs].forEach(allTab => allTab.style.display = 'none')
        }
        document.getElementById('block-' + e.target.getAttribute('data-tab')).style.display = 'block';
    }

    useEffect(() => {
        updateTabs();
    }, [tabs]);

    const updateTabs = () =>{
        let tabsObj = []
        if (tabs && tabs.length > 0) {
            tabs.map((tab, index) => {
                tabsObj.push({ 'id': tab.clientId, 'title': tab.attributes.tabTitle });
            })
        }
        setAttributes({
            tabItems: tabsObj
        })
    }

    if (tabItems.length < 1) {
        return (
            <div>
                <Placeholder
                    key="placeholder"
                    icon="editor-table"
                    label="Tab layout"
                    instructions="Kies je tabs"
                >
                    <InnerBlocks
                        allowedBlocks={ALLOWED_BLOCKS}
                    />
                </Placeholder>
            </div>
        )
    }

    return (
        <Fragment>
            <div className={className}>
                <nav className="c-tab-nav o-flex">
                    {tabItems &&
                        tabItems.map((tab, index) => {
                            return (
                                <a href="#" className="c-tab-nav-item c-button" onClick={clickTab} data-tab={tab.id}>
                                    {tab.title}
                                </a>
                            )
                        })
                    }
                </nav>
                <div class="c-tab-content">
                    <InnerBlocks
                        allowedBlocks={ALLOWED_BLOCKS}
                    />
                </div>
            </div>
        </Fragment>
    )
}