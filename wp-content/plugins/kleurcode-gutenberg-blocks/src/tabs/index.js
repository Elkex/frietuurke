import edit from './edit';
import save from './save';
import icons from '../utils/icons';

const { registerBlockType } = wp.blocks;

registerBlockType( 'kleurcode-core/tabs', {
	title: 'Tabs',
	icon: icons.kleurcode,
	category: 'kleurcode-core',
	keywords: [
		'tabs','tab'
	],
	supports: {
		html: false,
	},
	attributes: {
		tabItems: {
			type: 'array',
			default: []
		},
	},
	edit,
	save
} );
