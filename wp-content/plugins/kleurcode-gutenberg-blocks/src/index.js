/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

// CORE BLOCKS

import './blockstyles/index.js';
import './lists/index.js';
import './unregistered-blocks/index.js';


// CUSTOM BLOCKS
import './animation/index.js';
import './counter/index.js';
import './faq/index.js';
import './header/index.js';
import './parallax/index.js';
//import './recent-cases/index.js';
//import './select-cases/index.js';
import './slideshow/index.js'; 
import './tab/index.js';
import './tabs/index.js';
import './thumbnail-text/index.js';
