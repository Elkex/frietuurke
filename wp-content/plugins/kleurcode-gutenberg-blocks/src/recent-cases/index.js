import icons from '../utils/icons';

const { serverSideRender: ServerSideRender } = wp;

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {
	Fragment
} = wp.element;

const {
	SelectControl,
	TextControl,
	PanelBody,
	Placeholder
} = wp.components;

const {
	RichText,
	InspectorControls
} = wp.blockEditor;

registerBlockType('kleurcode-core/recent-cases', {
	title: 'Select recent cases',
	icon: icons.kleurcode,
	category: 'kleurcode-core',
	keywords: [
		'cases', 'posts'
	],
	attributes: {
		postType: {
			type: 'string',
		},
		postNum: {
			type: 'number',
			default: 3
		}
	},
	edit: function ({ attributes, setAttributes, className, isSelected }) {

		const {
			postType,
			postNum,
		} = attributes;

		const setPostType = (newPostType) => {
			setAttributes(
				{
					postType: newPostType
				}
			)
		}

		const setPostNum = (newPostNum) => {
			setAttributes(
				{
					postNum: parseInt(newPostNum)
				}
			)
		}

		if (document.querySelectorAll(".l-portfolio-thumbnail-btn").length > 0) {
			document.querySelector(".l-portfolio-thumbnail-btn").addEventListener("click", function (event) {
				event.preventDefault()
			});
		}

		return (
			<Fragment>
				<InspectorControls>
					<PanelBody title="Recent post settings" icon='none' initialOpen={true}>
						<SelectControl
							label={'Kies je categorie'}
							value={postType}
							onChange={setPostType}
							options={[
								{
									value: 0, label: 'Selecteer een post type', disabled: true
								},
								{
									value: 1, label: 'pages'
								},
								{
									value: 2, label: 'posts'
								},
								{
									value: 3, label: 'portfolio'
								}
							]}
						/>
						<TextControl
							type='number'
							value={postNum}
							onChange={setPostNum}
							label='Aantal post te tonen'
						/>
					</PanelBody>
				</InspectorControls>
				{!!postType ?
					<div className={className}>
						<ServerSideRender
							block="kleurcode-core/recent-cases"
							attributes={attributes}
						/>
					</div>
					:
					<Placeholder
						label="Kies je posttype in de sidebar."
						instructions="Kies je posttype in de sidebar en je thumbnails zullen hier verschijnen."
					/>
				}
			</Fragment>
		);
	},
	save: function () {
		return null;
	}
});
