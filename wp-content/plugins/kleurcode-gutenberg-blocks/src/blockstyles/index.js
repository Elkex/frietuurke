wp.domReady(function () {
     /**
     * UNREGISTER BLOCK STYLES
     */
    wp.blocks.unregisterBlockStyle('core/image', 'rounded');

    /**
    * REGISTER BLOCK STYLES
    */

     wp.blocks.registerBlockStyle( 'core/image', [ 
		{
			name: 'full',
			label: 'Volledige breedte',
		},
	]);
   
})