wp.domReady( () => {
	wp.blocks.unregisterBlockStyle(
		'core/list',
		[
			'checklist'
		]
	);

	wp.blocks.registerBlockStyle(
		'core/list',
		[
			{
				name: 'checklist',
				label: 'Checklist',
				isDefault: true,
			},
			{
				name: 'plus',
				label: 'Plus',
				isDefault: false,
			}
		]
	);
} );
