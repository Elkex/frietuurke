const {
    RichText,
    MediaPlaceholder,
    InspectorControls,
    getColorObjectByColorValue
} = wp.blockEditor;

const {
    TextControl,
    PanelBody,
    ColorPalette
} = wp.components;

export default function Edit(props) {
    const {
        attributes,
        setAttributes,
        className
    } = props;

    const {
        text,
        counter,
        image,
        imgId,
        colorSlug,
        colorHex
    } = attributes;

    let colors = wp.data.select("core/editor").getEditorSettings().colors

    const updateCounter = (newCounter) => {
        setAttributes({
            counter: parseInt(newCounter),
        });
    }

    const updateText = (newText) => {
        setAttributes({
            text: newText,
        });
    }

    const updateImage = (newImage) => {
        setAttributes({
            image: newImage,
        });
        setAttributes({
            imgId: newImage,
        });
    }

    const updateColor = (newColor) => {
        let colorClass = getColorObjectByColorValue(wp.data.select('core/editor').getEditorSettings().colors, newColor).slug;
        setAttributes({
            colorHex: newColor,
        });
        setAttributes({
            colorSlug: 'has-'+colorClass+'-color',
        });
    }

    return (
        <div className={'c-counter'}>
            <InspectorControls>
                <PanelBody header="Counter nummer">
                    <TextControl
                        type="number"
                        onChange={updateCounter}
                        value={counter}
                        label="Typ hier je counter nummer."
                    />
                    <ColorPalette
                        colors={colors}
                        onChange={updateColor}
                        value={colorHex}
                    />
                </PanelBody>
            </InspectorControls>
            <MediaPlaceholder
                onSelect={updateImage}
                allowedTypes={['image']}
                multiple={false}
                value={imgId}
                labels={!imgId ? { title: 'The Image' } : { title: '', instructions: '' }}
            />
            <figure className="g-flex g-flex-align-items-center">
                {image &&
                    <img src={image.url} className="c-counter-icon" />
                }
            </figure>
            <p className={'c-counter-number g-m-none ' + colorSlug}>
                {counter}
            </p>
            <RichText
                onChange={updateText}
                value={text}
                className={'c-counter-text ' + colorSlug}
                placeholder="Typ hier je tekst."
                tagName="p"
                formattingControls={['textColor']}
            />
        </div>
    )
}