import edit from './edit';
import save from './save';
import icons from '../utils/icons';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'kleurcode-core/counter', {
	title: 'Counter',
	icon: icons.kleurcode,
	category: 'kleurcode-core',
	keywords: [
		'counter','aftellen',
	],
	attributes: {
		text: {
			type: 'string'
		},
		counter: {
			type: 'number'
		},
		image: {
			type: 'object',
		},
		imgId: {
			type: 'object'
		},
		colorHex: {
			type: 'string'
		},
		colorSlug: {
			type: 'string'
		},
	},
	edit,
	save
} );
