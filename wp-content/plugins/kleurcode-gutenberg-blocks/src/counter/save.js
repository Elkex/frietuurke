const {
    RichText
} = wp.blockEditor;

export default function Save({ attributes }) {
    const {
        counter,
        image,
        text,
        colorSlug
    } = attributes;

    return (
        <div className={'c-counter'}>
            <figure className="g-flex g-flex-align-items-center">
                {image &&
                    <img src={image.url} className="c-counter-icon" />
                }
            </figure>
            <p className={'c-counter-placeholder ' + colorSlug}>
                {counter}
            </p>
            <p className={'c-counter-number g-m-none ' + colorSlug}>
                0
            </p>
            <p className={'c-counter-text ' + colorSlug}>
                {text}
            </p>
        </div>
    )
}