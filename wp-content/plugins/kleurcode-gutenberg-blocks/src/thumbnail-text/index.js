/**
 * Post Intro Type.
 */

const {
    registerPlugin
} = wp.plugins;

const {
    PluginPostStatusInfo,
    PluginDocumentSettingPanel
} = wp.editPost;

const {
    Fragment,
    useState,
} = wp.element;

const {
    Button,
    ColorPalette
} = wp.components;

const {
    useEntityProp
} = wp.coreData;

const {
    select,
    useSelect
} = wp.data;

const {
    getColorObjectByColorValue
} = wp.blockEditor;

const ctaText = (props) => {

    const {
        attributes
    } = props;

    const postType = useSelect(
        (select) => select('core/editor').getCurrentPostType(),
        []
    );
    const [meta, setMeta] = useEntityProp(
        'postType',
        postType,
        'meta'
    );

    const [
        thumbnailTextColorHex,
        setThumbnailTextColorHex
    ] = useState(meta['_thumbnail_text_color_hex'] || '');

    let colors = wp.data.select("core/editor").getEditorSettings().colors

    const updateThumbnailTextColor = (newThumbnailTextColor) => {
        let colorClass = getColorObjectByColorValue(wp.data.select('core/editor').getEditorSettings().colors, newThumbnailTextColor).slug;
        setThumbnailTextColorHex(newThumbnailTextColor)
        setMeta({ ...meta, '_thumbnail_text_color_hex': newThumbnailTextColor,  '_thumbnail_text_color_slug': colorClass });
    }

    return (
        <Fragment>
               <PluginDocumentSettingPanel
				className="klasse-sidebar-thumbnail-text-toggle"
				name="thumbnail-text-panel"
				title="Thumbnail tekst"
				initialOpen={ true }
				icon="none"
			>
               <ColorPalette
                        colors={colors}
                        onChange={updateThumbnailTextColor}
                        value={thumbnailTextColorHex}
                    />
            </PluginDocumentSettingPanel>
        </Fragment>
    );
};


registerPlugin('cta-text', {
    render: ctaText,
});

