// Import block dependencies.

import { get, includes, invoke, isUndefined, pickBy } from 'lodash';

const {
	compose,
} = wp.compose;

const {
	withSelect,
} = wp.data;

const {
	useEffect,
	useState,
	Fragment
} = wp.element;

const {
	SelectControl,
	PanelBody,
	PanelRow,
	Panel,
	Placeholder,
} = wp.components;

const {
	InspectorControls
} = wp.blockEditor;

import Thumbnails from './thumbnails'

function Edit(props) {

	const {
		attributes,
		setAttributes,
		className,
		placeholderPosts,
	} = props;

	const {
		postType,
		postTypeString,
		postIds,
		postIdsPlaceholder,
		postThumbnails,
	} = attributes;

	let post_type

	const setPostType = (newPostType) => {

		switch (newPostType) {
			case '1':
				post_type = 'page';
				break;
			case '2':
				post_type = 'post';
				break;
			case '3':
				post_type = 'portfolio';
				break;
		}

		setAttributes(
			{
				postTypeString: post_type
			}
		)

		setAttributes(
			{
				postType: newPostType
			}
		)
		setAttributes(
			{
				postIds: []
			}
		)
		setAttributes(
			{
				postThumbnails: []
			}
		)
	}

	const setThumbnails = (ids) => {
		setAttributes(
			{
				postIdsPlaceholder: ids
			}
		)
		setAttributes(
			{
				postIds: ids.map(String)
			}
		)
	}

	useEffect(() => {
		getPosts();
	}, [postIds]);

	let thumbnails = []

	const getPosts = () => {

		let thumbnail

		{
			postIds && postIds.forEach((post) => {
				thumbnail = wp.data.select('core').getEntityRecord('postType', postTypeString, post);
				if (thumbnail.featured_media === 0) {
					thumbnails.push({ 'id': thumbnail.id, 'title': thumbnail.title.rendered, 'link': thumbnail.link, 'imageTitle': '', 'image': '', 'ctaTxtColor': thumbnail.meta._thumbnail_text_color_slug ? thumbnail.meta._thumbnail_text_color_slug : '' })
					setAttributes({
						postThumbnails: [...thumbnails]
					})
				} else {
					getMedia(thumbnail)
				}
			})
		};
	}

	const getMedia = (postItem) => {
		fetch(`${wpApiSettings.root}${wpApiSettings.versionString}media/${postItem.featured_media}`)
			.then(response => response.json())
			.then(featuredImage => {
				thumbnails.push({ 'id': postItem.id, 'title': postItem.title.rendered, 'link': postItem.link, 'imageTitle': featuredImage.title.rendered, 'image': featuredImage.source_url, 'ctaTxtColor': postItem.meta._thumbnail_text_color_slug ? postItem.meta._thumbnail_text_color_slug : '' })
				setAttributes({
					postThumbnails: [...thumbnails]
				})
			});
	}

	return (
		<div className={className}>
			<InspectorControls>
				<Panel header="Recent post settings">
					<PanelBody title="Recent post settings" icon='none' initialOpen={true}>
						<PanelRow>
							<SelectControl
								label={'Kies je categorie'}
								value={postType}
								onChange={setPostType}
								options={[
									{
										value: 0, label: 'Selecteer een post type', disabled: true
									},
									{
										value: 1, label: 'pages'
									},
									{
										value: 2, label: 'posts'
									},
									{
										value: 3, label: 'portfolio'
									}
								]}
							/>
						</PanelRow>
						<PanelRow>
							{postType &&
								<SelectControl
									multiple
									label={'Kies je posts'}
									value={postIdsPlaceholder}
									onChange={setThumbnails}
									options={placeholderPosts}
								/>
							}
						</PanelRow>
					</PanelBody>
				</Panel>
			</InspectorControls>
			{!postThumbnails &&
				<Placeholder
					label="Kies je posttype en posts in de sidebar."
					instructions="Kies je posttype en posts in de sidebar en je thumbnails zullen hier verschijnen."
				/>
			}
			{postThumbnails &&
				<section className="l-portfolio js-masonry">
					<Thumbnails {...props} />
				</section>
			}
		</div>
	)
}


export default compose([

	withSelect((select, props) => {

		const {
			postTypeString
		} = props.attributes;

		let posts = []
		let placeholderPosts = [];

		const query = {
			orderby: 'title',
			order: 'asc',
			status: 'publish',
		}

		posts = select('core').getEntityRecords('postType', postTypeString, query);

		if (posts) {
			posts.forEach((post) => {
				placeholderPosts.push({ value: post.id, label: post.title.rendered });
			});
		}

		return (
			{
				placeholderPosts,
			}
		)
	}),
])(Edit);