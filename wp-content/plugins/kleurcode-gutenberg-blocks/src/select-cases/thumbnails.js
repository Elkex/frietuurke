const {
    Fragment
} = wp.element;

export default function Thumbnails(props) {

    const {
        attributes,
    } = props;

    const {
        postThumbnails
    } = attributes;


    return (
        <Fragment>
            {
                postThumbnails.map((postThumbnail, index) => {
                    return (
                        <div className="l-portfolio-thumbnail">
                            <div className="l-portfolio-item">
                                {postThumbnail.image &&
                                    <img src={postThumbnail.image} />
                                }
                                <div className="l-portfolio-content">
                                    <h4 className={'g-m-none has-' + postThumbnail.ctaTxtColor + '-color'}>
                                        {postThumbnail.title}
                                    </h4>
                                </div>
                                <a href={postThumbnail.link} title={'Ontdek het project' + postThumbnail.title + ' van Kleurcode'} className="l-portfolio-thumbnail-btn"></a>
                            </div>
                        </div>
                    )
                })
            }
        </Fragment>
    )
}