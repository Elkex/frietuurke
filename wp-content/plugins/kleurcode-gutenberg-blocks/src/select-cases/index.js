import edit from './edit';
import save from './save';
import icons from '../utils/icons';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'kleurcode-core/select-cases', {
	title: 'Select cases',
	icon: icons.kleurcode,
	category: 'kleurcode-core',
	keywords: [
		'vacatures','posts'
	],
	attributes: {
		postIds: {
			type: 'object', 
		},
		postIdsPlaceholder: {
			type: 'array', 
		},
		postType: {
			type: 'string',
		},
		postTypeString: {
			type: 'string',
		},
		postThumbnails: { 
			type: 'array',
		},
	},
	edit,
	save
} );
