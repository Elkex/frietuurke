/**
 * Voeg FAQ item toe
 */

// Import block dependencies.

export default function Save({ attributes }) {

	const {
		postThumbnails
	} = attributes

	return (
		<section className={'l-portfolio ' + attributes.className}>
			{postThumbnails &&
				<div className="js-masonry">
					{postThumbnails.map((thumbnail, index) => {
						return (
							<div className="l-portfolio-thumbnail">
								<div className="l-portfolio-item">
									{thumbnail.image &&
										<img src={thumbnail.image} />
									}
									<div className="l-portfolio-thumbnail-content">
										<h4 className={'g-m-none has-' + thumbnail.ctaTxtColor + '-color'}>
											{thumbnail.title}
										</h4>
									</div>
									<a href={thumbnail.link} title={'Ontdek het project' + thumbnail.title + ' van Kleurcode'} className="l-portfolio-thumbnail-btn"></a>
								</div>
							</div>
						)
					})}
				</div>
			}
		</section>
	);
};