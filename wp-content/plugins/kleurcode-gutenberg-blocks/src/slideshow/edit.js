// Import block dependencies.

const {
	compose,
} = wp.compose;

const {
	withSelect,
	select
} = wp.data;

const {
	InnerBlocks,
} = wp.blockEditor;

const {
	Component,
} = wp.element;

const MY_TEMPLATE = [
	['core/gallery'],
];

function Edit(props) {

    const {
        attributes,
        setAttributes,
        className,
        changedImages,
    } = props;

    const {
        images
    } = attributes;

    if (changedImages && changedImages.length > 0) {

        setAttributes({
            images: changedImages
        });

    }

    console.log(images);

    return (
        <div className={className}>
            <InnerBlocks template={MY_TEMPLATE} templateLock={"all"} />
        </div>
    )
}

export default compose([
	withSelect((select, block) => {

		let changedImages = [];

		const selectedBlock = select('core/block-editor').getBlocksByClientId(select('core/block-editor').getSelectedBlockClientId());
		const parentBlock = select('core/block-editor').getBlocksByClientId(select('core/block-editor').getBlockHierarchyRootClientId(select('core/block-editor').getSelectedBlockClientId()));

		if (selectedBlock[0]) {
			if (selectedBlock[0].name === "core/gallery" && parentBlock[0].name === "kleurcode-core/slideshow") {
				if (document.querySelector(".components-panel__body") !== null) {
					document.querySelector(".components-panel__body").style.display = "none";
					document.querySelector(".components-panel__body").style.visibility = "hidden";
					document.querySelector(".components-panel__body").style.opacity = "0";
				}
				if (document.querySelector(".is-opened") !== null) {
					document.querySelector(".is-opened").style.display = "none";
					document.querySelector(".is-opened").style.visibility = "hidden";
					document.querySelector(".is-opened").style.opacity = "0";
				}
                if (document.querySelector(".components-popover") !== null) {
                    document.querySelector(".components-popover").style.display = "none";
                    document.querySelector(".components-popover").style.visibility = "hidden";
                    document.querySelector(".components-popover").style.opacity = "0";
                }
			}
		}

		const parentBlockimg = select('core/block-editor').getBlocksByClientId(block.clientId)[0].innerBlocks[0];

		if (parentBlockimg) {
			changedImages = parentBlockimg.attributes.images;
		}

		return {
			changedImages,
		}
	})
])(Edit);