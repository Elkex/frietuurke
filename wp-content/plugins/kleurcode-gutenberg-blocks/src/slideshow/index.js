import edit from './edit';
import save from './save';
import icons from '../utils/icons';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'kleurcode-core/slideshow', {
	title: 'Slideshow',
	icon: icons.kleurcode,
	category: 'kleurcode-core',
	keywords: [
		'afbeelding','gallerij','slideshow'
	],
	attributes: {
		images: {
			type: 'array',
		},
	},
	edit,
	save
} );
