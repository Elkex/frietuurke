/**
 * Voeg FAQ item toe
 */

// Import block dependencies.

const {
	InnerBlocks,
} = wp.blockEditor;

export default function Save({ attributes }) {

	return (
		<section>
			<InnerBlocks.Content />
		</section>
	);
};