// Import block dependencies.

const {
    InnerBlocks,
    MediaPlaceholder,
    InspectorControls,
    getColorObjectByColorValue
} = wp.blockEditor;

const {
    PanelBody,
    SelectControl,
    ColorPalette,
    ToggleControl
} = wp.components;

export default function Edit(props) {

    const {
        attributes,
        setAttributes,
        className,
    } = props;

    const {
        image,
        height,
        negMargin,
        colorHexa,
        colorSlug,
        imgId,
        opacity
    } = attributes;

    let colors = wp.data.select('core/editor').getEditorSettings().colors;

    const updateImage = (newImage) => {
        setAttributes({
            image: newImage,
        });
        setAttributes({
            imgId: newImage,
        });
    }

    const updateHeight = (newHeight) => {
        setAttributes({
            height: newHeight,
        });
    }

    const updateMargin = (newMargin) => {
        setAttributes({
            negMargin: newMargin,
        });
    }

    const updateColor = (newColor) => {
        let colorClass = getColorObjectByColorValue(wp.data.select('core/editor').getEditorSettings().colors, newColor).slug;

        setAttributes({
            colorHexa: newColor,
        });
        setAttributes({
            colorSlug: colorClass,
        });
    }

    const updateOpacity = (newOpacity) => {
        setAttributes({
            opacity: newOpacity
        })
    }

    return (
        <div className={className}>
            <MediaPlaceholder
                onSelect={updateImage}
                allowedTypes={['image']}
                multiple={false}
                value={imgId}
                labels={{ title: 'The Image' }}
            />
            <header className={`p-header p-header-wrapper g-p-y-xxl ` + height + ' p-header-' + colorSlug + (opacity ? ' p-header-opacity' : '')}>
                <figure className="g-flex g-flex-align-items-center">
                    {image &&
                        <img src={image.url} className="p-header-img" />
                    }
                    <figcaption className="p-header-content">
                        <div className="g-container"> 
                            <InnerBlocks />
                        </div>
                    </figcaption>
                </figure>
            </header>

            <InspectorControls>
                <PanelBody title="Header">

                    <ColorPalette
                        colors={colors}
                        onChange={updateColor}
                        value={colorHexa}
                    />

                    <SelectControl
                        label={'Header hoogte'}
                        help={'Bepaal de hoogte van je header'}
                        value={(!height ? setAttributes({ height: 'p-header-wrapper-m' }) : height)}
                        onChange={updateHeight}
                        options={[
                            { label: 'Kies hoogte', value: null, disabled: true },
                            { label: 'Geen', value: '' },
                            { label: '200 px', value: 'p-header-wrapper-xs' },
                            { label: '300 px', value: 'p-header-wrapper-s' },
                            { label: '400 px', value: 'p-header-wrapper-m' },
                            { label: '600 px', value: 'p-header-wrapper-l' },
                        ]}
                    />
                    <SelectControl
                        label={'Header hoogte'}
                        help={'Bepaal de hoogte van je header'}
                        value={(!negMargin ? setAttributes({ negMargin: '' }) : negMargin)}
                        onChange={updateMargin}
                        options={[
                            { label: 'Geen', value: '' },
                            { label: 'Negatieve marge', value: 'p-header-neg' },
                        ]}
                    />
                    <ToggleControl
                        label="Transparante achtergrond"
                        help={opacity ? 'De achtergrond is transparant.' : 'De achtergrond is niet transparant.'}
                        checked={opacity}
                        onChange={updateOpacity}
                    />
                </PanelBody>
            </InspectorControls>
        </div>
    )
}