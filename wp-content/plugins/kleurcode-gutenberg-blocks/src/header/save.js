/**
 * Voeg Header toe
 */

// Import block dependencies.

const {
    InnerBlocks,
} = wp.blockEditor;

export default function Save({ attributes }) {

    const {
        image,
        height,
        negMargin,
        colorSlug,
        opacity
    } = attributes;

    return (
        <header className={'p-header p-header-wrapper g-p-y-xl ' + height + ' ' + negMargin + ' p-header-'+colorSlug + (opacity ? ' p-header-opacity' : '')}>
            <figure className="g-flex g-flex-align-items-center">
                {image &&
                    <img src={image.url} className="p-header-img" />
                }
                <figcaption className="p-header-content">
                    <div className="g-container">
                        <InnerBlocks.Content />
                    </div>
                </figcaption>
            </figure>
        </header>
    );
};