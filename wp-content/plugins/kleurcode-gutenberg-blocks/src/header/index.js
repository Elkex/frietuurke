import edit from './edit';
import save from './save';
import icons from '../utils/icons';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'kleurcode-core/header', {
	title: 'Header',
	icon: icons.kleurcode,
	category: 'kleurcode-core',
	keywords: [
		'header','hero'
	],
	attributes: {
		image: {
			type: 'object',
		},
		height: {
			type: 'string',
		},
		negMargin: {
			type: 'string'
		},
		colorHexa: {
			type: 'string'
		},
		colorSlug: {
			type: 'string'
		},
		imgId: {
			type: 'object'
		},
		opacity: {
			type: 'boolean'
		}
	},
	edit,
	save
} );
