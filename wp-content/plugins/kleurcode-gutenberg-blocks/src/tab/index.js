import edit from './edit';
import save from './save';
import icons from '../utils/icons';

const { registerBlockType } = wp.blocks;

registerBlockType( 'kleurcode-core/tab', {
	title: 'Tab',
	icon: icons.kleurcode,
	category: 'kleurcode-core',
	parent: [ 'kleurcode-core/tabs' ],
	keywords: [
		'tabs','tab'
	],
	supports: {
		html: false,
	},
	attributes: {
		tabTitle: {
			type: 'string', 
		},
		tabId: {
			type: 'string', 
		},
	},
	edit,
	save
} );
