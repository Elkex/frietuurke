const {
    Fragment,
} = wp.element;

const {
    InnerBlocks,
    InspectorControls,
} = wp.blockEditor;

const {
    PanelBody,
    PanelRow,
    TextControl,
} = wp.components;

export default function Edit(props) {
    const {
        attributes,
        setAttributes,
        className,
        clientId
    } = props;

    const {
        tabTitle,
        tabId
    } = attributes;

    const updateTabTitle = (newTabTitle) => {
        setAttributes({
            tabTitle: newTabTitle
        })
    }

    setAttributes({
        tabId: clientId
    })

    return (
        <Fragment>
            <InspectorControls>
                <PanelBody title="Voer tab titel in" icon='none' initialOpen={true}>
                    <PanelRow>
                        <TextControl
                            label="Tab titel"
                            value={tabTitle}
                            onChange={updateTabTitle}
                        />
                    </PanelRow>
                </PanelBody>
            </InspectorControls>
            <div className={attributes.className}>
                <InnerBlocks />
            </div>
        </Fragment>
    )
}