const {
    InnerBlocks,
} = wp.blockEditor;

export default function Save({ attributes }) {
    return (
		<section id={attributes.tabId} className={attributes.className, 'g-p-md'}>
            <InnerBlocks.Content />
        </section>
    )
}