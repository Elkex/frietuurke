// Import block dependencies.

const {
    InnerBlocks,
    MediaPlaceholder,
    InspectorControls,
    getColorObjectByColorValue
} = wp.blockEditor;

const {
    PanelBody,
    SelectControl,
    ColorPalette,
    ToggleControl
} = wp.components;

export default function Edit(props) {

    const {
        attributes,
        setAttributes,
        className,
    } = props;

    const {
        image,
        height,
        imgId,
    } = attributes;

    let colors = wp.data.select('core/editor').getEditorSettings().colors;

    const updateImage = (newImage) => {
        setAttributes({
            image: newImage,
        });
        setAttributes({
            imgId: newImage,
        });
    }

    const updateHeight = (newHeight) => {
        setAttributes({
            height: newHeight,
        });
    }

    let divStyle

    if (image) {
        let imgUrl = image.url
        divStyle  = {
                backgroundImage: "url(" + imgUrl + ")"
        }
    }

    return (
        <div className={className}>
            <MediaPlaceholder
                onSelect={updateImage}
                allowedTypes={['image']}
                multiple={false}
                value={imgId}
                labels={{ title: 'The Image' }}
            />
            <div style={divStyle} className={`c-parallax c-parallax-wrapper g-p-y-xxl ` + height}>
                <div className="g-container">
                    <InnerBlocks />
                </div>
            </div>

            <InspectorControls>
                <PanelBody title="Parallax">
                    <SelectControl
                        label={'Parallax hoogte'}
                        help={'Bepaal de hoogte van je parallax'}
                        value={(!height ? setAttributes({ height: 'c-parallax-wrapper-m' }) : height)}
                        onChange={updateHeight}
                        options={[
                            { label: 'Kies hoogte', value: null, disabled: true },
                            { label: 'Geen', value: '' },
                            { label: '200 px', value: 'c-parallax-wrapper-xs' },
                            { label: '300 px', value: 'c-parallax-wrapper-s' },
                            { label: '400 px', value: 'c-parallax-wrapper-m' },
                            { label: '600 px', value: 'c-parallax-wrapper-l' },
                        ]}
                    />
                </PanelBody>
            </InspectorControls>
        </div>
    )
}