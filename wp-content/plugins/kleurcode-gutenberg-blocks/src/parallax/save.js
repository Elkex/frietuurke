/**
 * Voeg Header toe
 */

// Import block dependencies.

const {
    InnerBlocks,
} = wp.blockEditor;

export default function Save({ attributes }) {

    const {
        image,
        height,
    } = attributes;

    let divStyle

    if (image) {
        let imgUrl = image.url
        divStyle  = {
                backgroundImage: "url(" + imgUrl + ")"
        }
    }

    return (
        <div style={divStyle} className={'c-parallax c-parallax-wrapper g-p-y-xxl ' + height}>
            <div className="g-container">
                <InnerBlocks.Content />
            </div>
        </div>
    );
};