import edit from './edit';
import save from './save';
import icons from '../utils/icons';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'kleurcode-core/parallax', {
	title: 'Parallax',
	icon: icons.kleurcode,
	category: 'kleurcode-core',
	keywords: [
		'parallax'
	],
	attributes: {
		image: {
			type: 'object',
		},
		height: {
			type: 'string',
		},
		imgId: {
			type: 'object'
		},
	},
	edit,
	save
} );
