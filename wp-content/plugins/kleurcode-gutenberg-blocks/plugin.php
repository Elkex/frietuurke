<?php

/**
 * Plugin Name: Kleurcode Core blocks
 * Description: Kleurcode Gutenberg blocks
 * Author: Elke Moras
 * Author URI: https://kleurcode.be
 * Version: 1.0.0
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 */

if (!defined('ABSPATH')) {
	exit;
}

/**
 * Block Initializer.
 */

function kleurcode_block_assets()
{ // phpcs:ignore

	$plugin_slug = 'kleurcode-gutenberg-blocks';

	// Automatically load dependencies and version.
	$asset_file = include plugin_dir_path(__FILE__) . 'build/index.asset.php';

	wp_enqueue_script(
		$plugin_slug,
		plugins_url(
			'build/index.js',
			__FILE__
		),
		$asset_file['dependencies'],
		$asset_file['version'],
		true
	);


	// Register block editor styles for backend.
	wp_enqueue_style(
		$plugin_slug . '-block-editor-styles',
		get_template_directory_uri() . '/assets/dist/css/styles.css',
		array(),
		null
	);

	$meta_fields = array(
		array(
			'name'        => '_thumbnail_text_color_hex',
			'type'        => 'string',
			'auth_cb'     => true,
			'object_type' => 'portfolio',
		),
		array(
			'name'        => '_thumbnail_text_color_slug',
			'type'        => 'string',
			'auth_cb'     => true,
			'object_type' => 'portfolio',
		),
	);

	foreach ($meta_fields as $field) {
		$args = array(
			'show_in_rest' => true,
			'single'       => true,
			'type'         => $field['type'],
		);

		if (true === $field['auth_cb']) {
			$args['auth_callback'] = '__return_' . (current_user_can('edit_posts') ? 'true' : 'false');
		}

		register_post_meta(
			$field['object_type'],
			$field['name'],
			$args
		);
	};

	/**
	 * Register block callbacks
	 */

	register_block_type(
		'kleurcode-core/slideshow',
		array(
			'editor_script' => 'kleurcode-block-js',
			'style'         => 'kleurcode-block-editor-css',
			'render_callback' =>  'render_slideshow',
			'attributes' => array(
				'images' => array(
					'type' => 'array'
				)
			)
		)
	);

	register_block_type(
		'kleurcode-core/recent-cases',
		array(
			'editor_script' => 'kleurcode-block-js',
			'style'         => 'kleurcode-block-editor-css',
			'render_callback' =>  'render_recent_cases',
			'attributes' => array(
				'postType' => array(
					'type' => 'string',
				),
				'postNum' => array(
					'type' => 'number',
				),
				'containerClassName' => array(
					'type' => 'boolean',
				),
				'thumbnailColorText' => array(
					'type' => 'string',
					'source' => 'meta',
					'meta' => '_thumbnail_text_color_slug'
				),
			)
		)
	);
}

add_action('enqueue_block_editor_assets', 'kleurcode_block_assets');

/**
 * Set block categories
 */

function kleurcode_block_categories($categories)
{
	$category_slugs = wp_list_pluck($categories, 'slug');
	return in_array('kleurcode', $category_slugs, true) ? $categories : array_merge(
		$categories,
		array(
			array(
				'slug'  => 'kleurcode-core',
				'title' => 'Kleurcode',
				'icon'  => null,
			),
		)
	);
}

add_filter('block_categories', 'kleurcode_block_categories');

/**
 * Render slideshow.
 *
 * @param array  $attributes Optional. Block attributes. Default empty array.
 * @param string $content    Optional. Block content. Default empty string.
 * @return string            Rendered block type output.
 */

function render_slideshow($attributes, $content)
{
	if (empty($attributes['images'])) {
		return '';
	}
	$block = "";
	$block .= '<section class="c-slider">';
	$block .= '<div class="g-container">';
	$block .= '<div class="c-slider c-slider-slick c-slider-slick-for o-block" ref="slickSlide">';
	foreach ($attributes['images'] as $image) {
		$block .= '<a data-fancybox="gallery" href="' . $image['url'] . '">';
		$block .= '<img src="' . $image['url'] . '" srcset="" alt="' . $image['alt'] . '" class="zoom" data-zoom-image="' . $image['url'] . '" />';
		$block .= '</a>';
	}
	$block .= '</div>';

	$block .= '<div class="c-slider-nav-container">';
	$block .= '<div class="c-slider c-slider-slick c-slider-slick-nav g-m-top-sm" ref="slickNav">';
	foreach ($attributes['images'] as $image) {
		$block .= '<div>';
		$block .= '<img class="" src="' . $image['url'] . '" srcset="" alt="' . $image['url'] . '" />';
		$block .= '</div>';
	}
	$block .= '</div>';
	$block .= '<button class="prev-arrow"></button>';
	$block .= '<button class="next-arrow"></button>';
	$block .= '</div>';
	$block .= '</div>';
	$block .= '</section>';

	return sprintf(
		$block
	);
}

function render_recent_cases($attributes, $content)
{
	switch ($attributes['postType']) {
		case '1':
			$post_type =  'page';
			break;
		case '2':
			$post_type =  'post';
			break;
		case '3':
			$post_type =  'portfolio';
			break;
	}

	$recent_posts = wp_get_recent_posts(
		array(
			'numberposts' => $attributes['postNum'],
			'post_status'  => 'publish',
			'post_type' => $post_type,
		)
	);
	$block = "";
	$block .= '<section class="l-portfolio' . ($attributes['containerClassName'] ? ' g-container' : '') . '">';
	$block .= '<div class="js-masonry">';
	foreach ($recent_posts as $post) {
		$post_id = $post['ID'];
		$block .= '<div class="l-portfolio-thumbnail">';
		$block .= '<div class="l-portfolio-item">';
		$block .= '<img src="' . get_the_post_thumbnail_url($post_id, "full") . '" />';
		$block .= '<div class="l-portfolio-thumbnail-content">';
		$block .= '<h4 class="g-m-none has-' . get_post_meta($post_id, '_thumbnail_text_color_slug', true) . '-color">' . get_the_title($post_id) . '</h4>';
		$block .= '</div>';
		$block .= '<a href="' . get_permalink($post_id) . '" class="l-portfolio-thumbnail-btn"></a>';
		$block .= '</div>';
		$block .= '</div>';
	}
	$block .= '</div>';
	$block .= '</section>';

	return $block;
}

function kleurcode_color_setup_features()
{

	add_theme_support('disable-custom-colors');

	add_theme_support('editor-color-palette', array(
		array(
			'name'  => __('Dark', 'kleurcode'),
			'slug'  => 'dark',
			'color'	=> '#010101',
		),
		array(
			'name'  => __('Grey', 'kleurcode'),
			'slug'  => 'grey',
			'color'	=> '#CCCCCC',
		),
		array(
			'name'  => __('Light', 'kleurcode'),
			'slug'  => 'light',
			'color'	=> '#F9F9F9',
		),
		array(
			'name'  => __('Primary', 'kleurcode'),
			'slug'  => 'primary',
			'color'	=> '#FFF265',
		),
	));

	add_theme_support(
		'editor-gradient-presets',
		array(
			array(
				'name'     => __('Pink to yellow', 'kleurcode'),
				'gradient' => 'linear-gradient(90deg, rgba(242,47,106,1) 0%, rgba(245,235,109,1) 100%)',
				'slug'     => 'pink-to-yellow'
			),
			array(
				'name'     => __('Yellow to pink', 'kleurcode'),
				'gradient' => 'linear-gradient(90deg, rgba(245,235,109,1) 0%, rgba(242,47,106,1) 100%)',
				'slug'     =>  'yellow-to-pink',
			),
			array(
				'name'     => __('Orange to purple', 'kleurcode'),
				'gradient' => 'linear-gradient(90deg, rgba(237,133,84,1) 0%, rgba(95,35,107,1) 100%)',
				'slug'     =>  'orange-to-purple',
			),
			array(
				'name'     => __('Purple to orange', 'kleurcode'),
				'gradient' => 'linear-gradient(90deg, rgba(95,35,107,1) 0%, rgba(237,133,84,1) 100%)',
				'slug'     =>  'purple-to-orange',
			),

		)
	);

	add_theme_support('wp-block-styles');
	add_editor_style('editor-style.css');

	add_theme_support('block-templates');
	add_theme_support('block-template-parts');
}

add_action('after_setup_theme', 'kleurcode_color_setup_features');
